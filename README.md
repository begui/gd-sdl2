# gd-sdl2

Common SDL2 functions with a modern c++ twist.

## Sandbox

The sandbox project doesn't utilize any of the other SDL2 libraries, only it's core.

To build it

    cmake ../ -DGD_SDL2_BUILD_SANDBOX=1

### Emscripten

To build with emscrpten. Source your **emsdk_env.sh**

    emcmake cmake ../ -DGD_SDL2_BUILD_SANDBOX=1
    emmake make
    emrun sandbox/sandbox.html


### OpenDingux

    cmake ../ -DGD_SDL2_BUILD_SANDBOX=1 -DGD_SDL2_BUILD_OD=RG350
    make && (cd sandbox/snake/ && ./create_opk.sh && scp gd_sdl2_snake.opk od@10.1.1.2:/media/data/apps ) && (cd sandbox/doomfire/ && ./create_opk.sh && scp gd_sdl2_doomfire.opk od@10.1.1.2:/media/data/apps)


