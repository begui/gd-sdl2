#include "game.hh"

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#endif

#include <algorithm>
#include <cmath>
#include <random>

SDL_Window *Game::window_;
SDL_Renderer *Game::renderer_;

SDL_Window *Game::window ( ) { return Game::window_; }
SDL_Renderer *Game::renderer ( ) { return Game::renderer_; }

void Game::setWindow ( SDL_Window *window ) { Game::window_ = window; }
void Game::setRenderer ( SDL_Renderer *renderer ) { Game::renderer_ = renderer; }

//
// Lambda Functions
//
auto spawnFoodLoc = [] ( int XX, int YY, int multiple ) {
  static std::random_device rd;
  static std::mt19937 gen ( rd ( ) );
  static std::uniform_int_distribution< int > x ( 0, XX );
  static std::uniform_int_distribution< int > y ( 0, YY );
  //
  auto roundToNearestCell = [] ( int num, int mul ) { return ( ( ( num + mul - 1 ) ) / mul ) * mul; };

  return std::make_pair ( roundToNearestCell ( x ( gen ), multiple ), roundToNearestCell ( y ( gen ), multiple ) );
};

auto isSnakeCollision = [] ( auto *game ) {
  for ( std::size_t i{ 1 }; i < game->snake_.cells.size ( ); ++i ) {
    if ( game->snake_.cells[ 0 ].first == game->snake_.cells[ i ].first &&
         game->snake_.cells[ 0 ].second == game->snake_.cells[ i ].second ) {
      return true;
    }
  }
  return false;
};

auto isWallCollision = [] ( auto *game, auto width, auto height ) {
  return game->snake_.cells[ 0 ].first < 0 || game->snake_.cells[ 0 ].second < 0 ||
         game->snake_.cells[ 0 ].first >= width || game->snake_.cells[ 0 ].second >= height;
};

auto isFoodCollision = [] ( auto *game ) {
  return game->foodPosition_.first == game->snake_.cells[ 0 ].first &&
         game->foodPosition_.second == game->snake_.cells[ 0 ].second;
};

void event_loop ( Game *game ) {
  //
  // Input
  //
  float swipeX{ 0.f };
  float swipeY{ 0.f };
  SDL_Event event;
  while ( SDL_PollEvent ( &event ) ) {
    if ( event.type == SDL_QUIT ) {
      game->stop ( );
    }
    if ( event.type == SDL_KEYDOWN ) {
      if ( 0 == event.key.repeat ) {
        auto sym = event.key.keysym.sym;
        switch ( sym ) {
          case SDLK_ESCAPE:
#ifdef __EMSCRIPTEN__
            game->reset ( );
#else
            game->stop ( );
#endif
            break;
          default:
            break;
        }

        // Note: Bug here if you left - up - right too quickly
        const auto dUp = SDLK_UP == sym;
        const auto dDown = SDLK_DOWN == sym;
        const auto dLeft = SDLK_LEFT == sym;
        const auto dRight = SDLK_RIGHT == sym;

        if ( dUp && !game->snake_.isDirectionDown ( ) ) {
          game->snake_.direction = DirectionType::UP;
        } else if ( dDown && !game->snake_.isDirectionUp ( ) ) {
          game->snake_.direction = DirectionType::DOWN;
        } else if ( dLeft && !game->snake_.isDirectionRight ( ) ) {
          game->snake_.direction = DirectionType::LEFT;
        } else if ( dRight && !game->snake_.isDirectionLeft ( ) ) {
          game->snake_.direction = DirectionType::RIGHT;
        }
      }
    } else if ( event.type == SDL_FINGERUP || event.type == SDL_FINGERDOWN || event.type == SDL_FINGERMOTION ) {
      const auto tf = event.tfinger;
      if ( SDL_FINGERUP == event.type ) {
        const auto sx = std::fabs ( swipeX );
        const auto sy = std::fabs ( swipeY );

        const auto dUp = swipeY < 0;
        const auto dDown = swipeY > 0;
        const auto dLeft = swipeX < 0;
        const auto dRight = swipeX > 0;

        if ( std::isgreater ( sy, sx ) ) {
          if ( dUp && !game->snake_.isDirectionDown ( ) ) {
            game->snake_.direction = DirectionType::UP;
          }
          if ( dDown && !game->snake_.isDirectionUp ( ) ) {
            game->snake_.direction = DirectionType::DOWN;
          }
        } else if ( std::isgreater ( sx, sy ) ) {
          if ( dLeft && !game->snake_.isDirectionRight ( ) ) {
            game->snake_.direction = DirectionType::LEFT;
          }
          if ( dRight && !game->snake_.isDirectionLeft ( ) ) {
            game->snake_.direction = DirectionType::RIGHT;
          }
        }
      } else if ( SDL_FINGERDOWN == event.type ) {
        swipeX = 0.f;
        swipeY = 0.f;
      } else if ( SDL_FINGERMOTION == event.type ) {
        swipeX += tf.dx;
        swipeY += tf.dy;
      } else {
      }
    } else {
    }
  }  // end of event while loop
  //
  // Update
  //

  auto newTick = SDL_GetTicks ( ) / game->tick_;
  if ( newTick > game->frameNum_ ) {
    game->frameNum_ = newTick;
    auto dirX{ 0 };
    auto dirY{ 0 };
    switch ( game->snake_.direction ) {
      case DirectionType::UP:
        dirX = 0;
        dirY = -1;
        break;
      case DirectionType::DOWN:
        dirX = 0;
        dirY = 1;
        break;
      case DirectionType::LEFT:
        dirX = -1;
        dirY = 0;
        break;
      case DirectionType::RIGHT:
        dirX = 1;
        dirY = 0;
        break;
      default:
        break;
    }
    const auto cursor_x = game->snake_.cells[ 0 ].first;
    const auto cursor_y = game->snake_.cells[ 0 ].second;
    game->grid_x = static_cast< int > ( cursor_x / game->box_offset );
    game->grid_y = static_cast< int > ( cursor_y / game->box_offset );

    const auto xval = ( game->box_offset ) * dirX;
    const auto yval = ( game->box_offset ) * dirY;

    // check for collision
    if ( isWallCollision ( game, game->width_, game->height_ ) || isSnakeCollision ( game ) ) {
      game->reset ( );
    } else {
      if ( isFoodCollision ( game ) ) {
        // generate new food location
        ++game->score;
        game->tick_ = std::clamp ( game->tick_ - 5, tick_min, tick_max );
        game->foodPosition_ =
            spawnFoodLoc ( game->width_ - game->box_offset, game->height_ - game->box_offset, game->box_offset );
      } else {
        // remove Snake Tail
        if ( game->snake_.cells.size ( ) >= 1 ) {
          game->snake_.cells.pop_back ( );
        }
      }
      // Ad Snake Head
      game->snake_.cells.emplace_front ( cursor_x + xval, cursor_y + yval );
    }

    // Add fading effect to square
    game->grid[ game->grid_x ][ game->grid_y ] += 5;
    for ( int y = 0; y < box_count_y; y++ ) {
      for ( int x = 0; x < box_count_x; x++ ) {
        game->temp_grid[ x ][ y ] = game->grid[ x ][ y ];
      }
    }

    for ( int y = 0; y < box_count_y; y++ ) {
      for ( int x = 0; x < box_count_x; x++ ) {
        float offset = -game->temp_grid[ x ][ y ] + ( game->temp_grid[ ( x + box_max_x ) % box_count_x ][ y ] +
                                                      game->temp_grid[ ( x + 1 ) % box_count_x ][ y ] +
                                                      game->temp_grid[ x ][ ( y + box_max_y ) % box_count_y ] +
                                                      game->temp_grid[ x ][ ( y + 1 ) % box_count_y ] ) /
                                                        8;

        game->grid[ x ][ y ] += ( offset / 350 ) * 60;
        if ( game->grid[ x ][ y ] > 1000 ) game->grid[ x ][ y ] = 1000;
        if ( game->grid[ x ][ y ] < 0 ) game->grid[ x ][ y ] = 0;
      }
    }
  }

  //
  // Rendering
  //

  sdl2::clear ( Game::renderer ( ) );

  // Render the grid
  for ( int y = 0; y < box_count_y; y++ ) {
    for ( int x = 0; x < box_count_x; x++ ) {
      //
      int red_mod = ( int ) ( game->grid[ x ][ y ] * 2.2 );
      int blue_mod = ( 15 - red_mod * 10 );
      if ( blue_mod < 0 ) blue_mod = 0;
      if ( red_mod > 220 ) red_mod = 220;
      //
      const std::uint8_t red = static_cast< std::uint8_t > ( 20 + red_mod );
      const std::uint8_t green = 20;
      const std::uint8_t blue = static_cast< std::uint8_t > ( 20 + ( uint8_t ) blue_mod );

      const sdl2::Recti32_t *box = &game->boxes[ static_cast< size_t > ( y * box_count_x + x ) ];
      sdl2::render_rect_fill ( Game::renderer ( ), *box, { red, green, blue, 255 } );
    }
  }

  auto boxsize = game->box_dimension;
  // Render Food
  sdl2::render_rect_fill ( Game::renderer ( ),
                           sdl2::Recti32_t { game->foodPosition_.first, game->foodPosition_.second, boxsize, boxsize },
                           { 0, 255, 0, 255 } );
  // Render Snake
  for ( const auto &p : game->snake_.cells ) {
    sdl2::render_rect_fill ( Game::renderer ( ), sdl2::Recti32_t{ p.first, p.second, boxsize, boxsize }, { 0, 0, 255, 255 } );
  }


  //
  // Swap the window
  //
  sdl2::swap ( Game::renderer ( ) );
}

void Game::run ( ) {
  const auto [ w, h ] = sdl2::get_render_logical_size ( Game::renderer ( ) );
  width_ = w;
  height_ = h;
  const auto offset = 2;
  box_dimension = ( int ) std::floor ( ( float ) width_ / box_count_x ) - offset;
  box_offset = box_dimension + offset;
  // set the render color
  for ( int y = 0; y < box_count_y; y++ ) {
    for ( int x = 0; x < box_count_x; x++ ) {
      boxes.push_back ( { x * box_offset, y * box_offset, box_dimension, box_dimension } );
      grid[ x ][ y ] = 0;
    }
  }
  foodPosition_ = spawnFoodLoc ( width_ - box_dimension, height_ - box_dimension, box_offset );
  snake_.cells.emplace_front ( 0, 0 );

#ifdef __EMSCRIPTEN__
  emscripten_set_main_loop_arg ( ( em_arg_callback_func ) event_loop, this, 0, 1 );
#else
  while ( running_ ) {
    event_loop ( this );
  }
#endif
}

void Game::stop ( ) {
  running_ = false;
#ifdef __EMSCRIPTEN__
  emscripten_cancel_main_loop ( );
#endif
}

void Game::reset ( ) {
  snake_.direction = DirectionType::DOWN;
  snake_.cells = { { 0, 0 } };
  tick_ = tick_max;
}
