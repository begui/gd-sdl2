#pragma once

#include <deque>

#include "gd/gd_sdl2.hh"

constexpr uint32_t tick_max = 250;
constexpr uint32_t tick_min = 100;

#ifdef WITH_GCW0
constexpr int box_count_x{ 32 };
constexpr int box_count_y{ ( box_count_x * 3 ) / 4 };
#else
// constexpr int box_count_x{25};
// constexpr int box_count_y{( box_count_x * 16 ) / 25};

constexpr int box_count_x{ 32 };
constexpr int box_count_y{ ( box_count_x * 3 ) / 4 };

#endif
constexpr int box_max_x{ box_count_x };
constexpr int box_max_y{ box_count_y };

enum class DirectionType { UP, DOWN, LEFT, RIGHT };
using Pair = std::pair< int32_t, int32_t >;
struct Snake {
  DirectionType direction{ DirectionType::DOWN };
  std::deque< Pair > cells{ { 0, 0 } };

 public:
  bool isDirectionUp ( ) const noexcept { return DirectionType::UP == direction; }
  bool isDirectionDown ( ) const noexcept { return DirectionType::DOWN == direction; }
  bool isDirectionLeft ( ) const noexcept { return DirectionType::LEFT == direction; }
  bool isDirectionRight ( ) const noexcept { return DirectionType::RIGHT == direction; }
};

class Game {
 public:
  void run ( );
  void stop ( );
  void reset ( );

 public:
  static SDL_Window *window ( );
  static SDL_Renderer *renderer ( );
  static void setWindow ( SDL_Window *window );
  static void setRenderer ( SDL_Renderer *renderer );

 private:
  static SDL_Window *window_;
  static SDL_Renderer *renderer_;

 private:
  friend void event_loop ( Game *game );

 private:
  bool running_{ true };
  int32_t width_;
  int32_t height_;

 private:
  uint32_t tick_{ tick_max };
  uint32_t frameNum_{ 0u };

 public:
  Snake snake_;
  Pair foodPosition_;
  int32_t score{ 0 };

 public:
  float grid[ box_count_x ][ box_count_y ];
  float temp_grid[ box_count_x ][ box_count_y ];
  int grid_x;
  int grid_y;
  int box_dimension;
  int box_offset;

  std::vector< sdl2::Recti32_t > boxes;
};
