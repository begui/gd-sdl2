#include <cstdlib>

#include "game.hh"

int main ( [[maybe_unused]] int argc, [[maybe_unused]] char *argv[] ) {
  //
  // Init sdl
  //
  auto initRtn = sdl2::init ( );
  if ( initRtn != 0 ) {
    sdl2::log::error ( sdl2::log::CategoryType::Application, "Failed to init SDL2. Init returned %d", initRtn );
    return EXIT_FAILURE;
  }

#if BUILD_GCW0
  constexpr auto WIDTH{ 320 };
  SDL_SetHint ( SDL_HINT_RENDER_DRIVER, "opengles" );
#elif BUILD_RG350
  constexpr auto WIDTH{ 640 };
  SDL_SetHint ( SDL_HINT_RENDER_DRIVER, "opengles" );
#else
  constexpr auto WIDTH{ 800 };
#endif
  constexpr auto HEIGHT{ ( WIDTH * 3 ) / 4 };

  sdl2::WindowUniqPtr window     = sdl2::create_window (  WIDTH, HEIGHT );
  sdl2::RendererUniqPtr renderer = sdl2::create_renderer ( window.get ( ) );
  sdl2::set_window_title ( window.get ( ), "Snake" );
  sdl2::set_render_logical_size ( renderer.get ( ), WIDTH, HEIGHT );
  sdl2::print_info ( );
  sdl2::print_renderer_info ( renderer.get ( ) );
  sdl2::print_window_info ( window.get ( ) );
  //
  // Start the game
  //
  Game game;
  game.setWindow ( window.get ( ) );
  game.setRenderer ( renderer.get ( ) );

  game.run ( );

  return EXIT_SUCCESS;
}
