#include <chrono>
#include <cstdlib>
#include <iostream>
#include <random>

#include "gd/gd_sdl2.hh"

#if BUILD_GCW0
constexpr auto WIDTH{ 320 };
#elif BUILD_RG350
constexpr auto WIDTH{ 640 };
#else
constexpr auto WIDTH{ 800 };
#endif
constexpr auto HEIGHT{ ( WIDTH * 3 ) / 4 };

struct Sdl2 {
  Sdl2 ( ) { sdl2::init ( ); }
  virtual ~Sdl2 ( ) = default;
  sdl2::WindowUniqPtr window{ nullptr };
  sdl2::RendererUniqPtr renderer{ nullptr };
};

void init ( Sdl2& sdl2 ) {
  sdl2.window   = sdl2::create_window ( WIDTH, HEIGHT );
  sdl2.renderer = sdl2::create_renderer ( sdl2.window.get ( ) );
  sdl2::set_window_title ( sdl2.window.get ( ), "Starfield" );
  sdl2::print_info ( );
}

struct Star {
  Star ( ) = default;
  // Star( float xx, float yy, uint8_t ccolor ) : x{ xx }, y{ yy }, color{ ccolor } { }
  float x{ 0 };
  float y{ 0 };
  uint8_t color{ 0x00 };

  static const size_t STAR_SIZE{ 1000 };
  static auto calculateInitialPosition ( int32_t dim ) {
    // yea yea ya.. it's not random
    return 1.0f * static_cast< float > ( ( rand ( ) % dim ) );
  }
  static auto calculateNewPoint ( float a, int32_t dim, float color ) {
    float halfDim = static_cast< float > ( dim ) / 2.f;
    return ( a - halfDim ) * ( color * 0.00001f + 1 ) + halfDim;
  };
};

//
// Main
//
int main ( [[maybe_unused]] int argc, [[maybe_unused]] char* argv[] ) {
  try {
    Sdl2 sdl2;

#if BUILD_GCW0 || BUILD_RG350
    SDL_SetHint ( SDL_HINT_RENDER_DRIVER, "opengles" );
#endif
    init ( sdl2 );

    std::vector< Star > stars ( Star::STAR_SIZE );
    for ( auto&& star : stars ) {
      star.x     = Star::calculateInitialPosition ( WIDTH );
      star.y     = Star::calculateInitialPosition ( HEIGHT );
      star.color = 0x00;
    }

    bool running{ true };
    while ( running ) {
      SDL_Event event;
      while ( SDL_PollEvent ( &event ) != 0 ) {
        switch ( event.type ) {
          case SDL_QUIT:
            running = false;
            break;
          case SDL_KEYDOWN: {
            switch ( event.key.keysym.sym ) {
              case SDLK_ESCAPE:
                running = false;
                break;
              default:
                break;
            }
          } break;
        }
      }  // end of while

      // Update

      for ( auto& star : stars ) {
        if ( star.color < 0xff ) {
          star.color += 1;
        }
        star.x = Star::calculateNewPoint ( star.x, WIDTH, star.color );
        star.y = Star::calculateNewPoint ( star.y, HEIGHT, star.color );
        // If we are out of screen boundry
        if ( star.x < 0 || star.x > WIDTH || star.y < 0 || star.y > HEIGHT ) {
          // create a new star
          star.x     = Star::calculateInitialPosition ( WIDTH );
          star.y     = Star::calculateInitialPosition ( HEIGHT );
          star.color = 0x00;
        }
      }
      // Render
      sdl2::clear ( sdl2.renderer.get ( ) );
      for ( const auto& star : stars ) {
        sdl2::render_point ( sdl2.renderer.get ( ), sdl2::Pointf_t{ star.x, star.y },
                             { star.color, star.color, star.color, 255 } );
      }
      sdl2::swap ( sdl2.renderer.get ( ) );
    }  // end of while
  } catch ( const std::exception& e ) {
    sdl2::log::error ( sdl2::log::CategoryType::Application, e.what ( ) );
  }
  return EXIT_SUCCESS;
}
