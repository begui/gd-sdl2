#include <cstdlib>

#include "gd/gd_sdl2.hh"

#if BUILD_GCW0
constexpr auto WIDTH{ 320 };
#elif BUILD_RG350
constexpr auto WIDTH{ 640 };
#else
constexpr auto WIDTH{ 800 };
#endif
constexpr auto HEIGHT{ ( WIDTH * 3 ) / 4 };

struct Sdl2 {
  Sdl2 ( ) { sdl2::init ( ); }
  virtual ~Sdl2 ( ) = default;
  sdl2::WindowUniqPtr window{ nullptr };
  sdl2::RendererUniqPtr renderer{ nullptr };
};

void init ( Sdl2& sdl2 ) {
  sdl2.window   = sdl2::create_window ( WIDTH, HEIGHT );
  sdl2.renderer = sdl2::create_renderer ( sdl2.window.get ( ) );
  sdl2::set_window_title ( sdl2.window.get ( ), "Hello" );
  sdl2::print_info ( );
}

//
// Main
//
int main ( [[maybe_unused]] int argc, [[maybe_unused]] char* argv[] ) {
  try {
    Sdl2 sdl2;
#if BUILD_GCW0 || BUILD_RG350
    SDL_SetHint ( SDL_HINT_RENDER_DRIVER, "opengles" );
#endif
    init ( sdl2 );

    bool running{ true };
    while ( running ) {
      SDL_Event event;
      while ( SDL_PollEvent ( &event ) != 0 ) {
        switch ( event.type ) {
          case SDL_QUIT:
            running = false;
            break;
          case SDL_KEYDOWN: {
            if ( SDLK_ESCAPE == event.key.keysym.sym ) {
              running = false;
            }
          } break;
          default:
            break;
        }
      }  // end of while
      // The Meat

      sdl2::clear ( sdl2.renderer.get ( ) );

      sdl2::render_point ( sdl2.renderer.get ( ), sdl2::Pointi32_t{ 90, 80 }, { 255, 0, 0, 255 } );
      sdl2::render_point ( sdl2.renderer.get ( ), sdl2::Pointf_t{ 110.f, 80.f }, { 0, 255, 0, 255 } );
      sdl2::render_circle ( sdl2.renderer.get ( ), sdl2::Pointi32_t{ 100, 100 }, 50.f, { 0, 0, 255, 255 } );

      sdl2::swap ( sdl2.renderer.get ( ) );

    }  // end of while
  } catch ( const std::exception& e ) {
    sdl2::log::error ( sdl2::log::CategoryType::Application, e.what() );
  }
  return EXIT_SUCCESS;
}
