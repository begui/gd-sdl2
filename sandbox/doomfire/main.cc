#include <chrono>
#include <cstdlib>
#include <iostream>
#include <random>
#include <array>

#include "gd/gd_sdl2.hh"

#if BUILD_GCW0
constexpr auto WIDTH{ 320 };
#elif BUILD_RG350
constexpr auto WIDTH{ 640 };
#else
constexpr auto WIDTH{ 800 };
#endif
constexpr auto HEIGHT{ ( WIDTH * 3 ) / 4 };

// constexpr auto FIRE_WIDTH{ WIDTH >> 1 };
// constexpr auto FIRE_HEIGHT{ HEIGHT >> 1 };

constexpr auto FIRE_WIDTH{ WIDTH };
constexpr auto FIRE_HEIGHT{ HEIGHT };

constexpr uint8_t rgbs[] = { 0x07, 0x07, 0x07,  //
                                  0x1F, 0x07, 0x07,  //
                                  0x2F, 0x0F, 0x07,  //
                                  0x47, 0x0F, 0x07,  //
                                  0x57, 0x17, 0x07,  //
                                  0x67, 0x1F, 0x07,  //
                                  0x77, 0x1F, 0x07,  //
                                  0x8F, 0x27, 0x07,  //
                                  0x9F, 0x2F, 0x07,  //
                                  0xAF, 0x3F, 0x07,  //
                                  0xBF, 0x47, 0x07,  //
                                  0xC7, 0x47, 0x07,  //
                                  0xDF, 0x4F, 0x07,  //
                                  0xDF, 0x57, 0x07,  //
                                  0xDF, 0x57, 0x07,  //
                                  0xD7, 0x5F, 0x07,  //
                                  0xD7, 0x5F, 0x07,  //
                                  0xD7, 0x67, 0x0F,  //
                                  0xCF, 0x6F, 0x0F,  //
                                  0xCF, 0x77, 0x0F,  //
                                  0xCF, 0x7F, 0x0F,  //
                                  0xCF, 0x87, 0x17,  //
                                  0xC7, 0x87, 0x17,  //
                                  0xC7, 0x8F, 0x17,  //
                                  0xC7, 0x97, 0x1F,  //
                                  0xBF, 0x9F, 0x1F,  //
                                  0xBF, 0x9F, 0x1F,  //
                                  0xBF, 0xA7, 0x27,  //
                                  0xBF, 0xA7, 0x27,  //
                                  0xBF, 0xAF, 0x2F,  //
                                  0xB7, 0xAF, 0x2F,  //
                                  0xB7, 0xB7, 0x2F,  //
                                  0xB7, 0xB7, 0x37,  //
                                  0xCF, 0xCF, 0x6F,  //
                                  0xDF, 0xDF, 0x9F,  //
                                  0xEF, 0xEF, 0xC7,  //
                                  0xFF, 0xFF, 0xFF };

constexpr uint32_t rgbSize{ sizeof ( rgbs ) / sizeof ( uint8_t ) };
constexpr uint32_t colorPaletteSize{ rgbSize / 3 };

SDL_Color colorPalette[ colorPaletteSize ];
std::array< uint32_t, FIRE_WIDTH * FIRE_HEIGHT > firePixels;

struct Sdl2 {
  Sdl2 ( ) { sdl2::init ( ); }
  virtual ~Sdl2 ( ) = default;
  sdl2::WindowUniqPtr window{ nullptr };
  sdl2::RendererUniqPtr renderer{ nullptr };
  sdl2::TextureUniqPtr fireTexture{ nullptr };
  sdl2::PixelFormatUniqPtr pixelFormat{ nullptr };
};

void init ( Sdl2& sdl2 ) {
  sdl2.window = sdl2::create_window (  WIDTH, HEIGHT  );
  sdl2.renderer = sdl2::create_renderer ( sdl2.window.get ( ) );
  sdl2::set_window_title ( sdl2.window.get ( ), "DoomFirePSX" );
  sdl2::print_info ( );

  sdl2.fireTexture = sdl2::create_texture< sdl2::TextureUniqPtr > (  //
      sdl2.window.get ( ),                                           //
      sdl2.renderer.get ( ),                                         //
      sdl2::TextureAccessType::Streaming,                            //
      FIRE_WIDTH, FIRE_HEIGHT );
  sdl2.pixelFormat = sdl2::create_pixelformat ( sdl2.window.get ( ) );

  // Init the color palette
  for ( auto i{ 0U }; i < colorPaletteSize; ++i ) {
    colorPalette[ i ].r = rgbs[ i * 3 + 0 ];
    colorPalette[ i ].g = rgbs[ i * 3 + 1 ];
    colorPalette[ i ].b = rgbs[ i * 3 + 2 ];
  }

  firePixels.fill ( 0 );
  for ( auto i{ 0U }; i < FIRE_WIDTH; ++i ) {
    firePixels[ ( FIRE_HEIGHT - 1 ) * FIRE_WIDTH + i ] = 36;
  }
}

//
// Main
//
int main ( [[maybe_unused]] int argc, [[maybe_unused]] char* argv[] ) {
  std::random_device random_device;
  std::mt19937 gen ( random_device ( ) );
  std::uniform_int_distribution<> dis ( 0, 3 );
  // return dis ( gen );

  try {
    Sdl2 sdl2;

#if BUILD_GCW0 || BUILD_RG350
    SDL_SetHint ( SDL_HINT_RENDER_DRIVER, "opengles" );
#endif
    init ( sdl2 );

    bool running{ true };
    while ( running ) {
      SDL_Event event;
      while ( SDL_PollEvent ( &event ) != 0 ) {
        switch ( event.type ) {
          case SDL_QUIT:
            running = false;
            break;
          case SDL_KEYDOWN: {
            switch ( event.key.keysym.sym ) {
              case SDLK_ESCAPE:
                running = false;
                break;
              default:
                break;
            }
          } break;
        }
      }  // end of while
      // The Meat
      for ( auto i{ 0U }; i < FIRE_WIDTH; ++i ) {
        for ( auto j{ 1U }; j < FIRE_HEIGHT; ++j ) {
          uint32_t srcIndex = j * FIRE_WIDTH + i;

          auto pixel = firePixels[ srcIndex ];
          if ( pixel == 0 ) {
            firePixels[ srcIndex - FIRE_WIDTH ] = 0;
          } else {
            auto randIndex = static_cast< uint32_t > ( dis ( gen ) );
            auto dstIndex = srcIndex - randIndex + 1;
            firePixels[ dstIndex - FIRE_WIDTH ] = pixel - ( randIndex & 1 );
          }
        }
      }
      sdl2::clear ( sdl2.renderer.get ( ) );
      sdl2::render_to_pixel (
          [ &sdl2 ] ( uint32_t* pixels ) {
            //
            for ( auto j{ 0U }; j < FIRE_HEIGHT; ++j ) {
              for ( auto i{ 0U }; i < FIRE_WIDTH; ++i ) {
                auto index = j * FIRE_WIDTH + i;
                auto colorIndex = firePixels[ index ];
                if ( colorIndex < colorPaletteSize ) {
                  pixels[ index ] = SDL_MapRGB ( sdl2.pixelFormat.get ( ),
                                                 colorPalette[ colorIndex ].r,  //
                                                 colorPalette[ colorIndex ].g,  //
                                                 colorPalette[ colorIndex ].b );
                }
              }
            }
          },
          sdl2.fireTexture.get ( ) );

      static const sdl2::Recti32_t rect{ ( WIDTH - FIRE_WIDTH ) / 2, ( HEIGHT - FIRE_HEIGHT ) / 2, FIRE_WIDTH, FIRE_HEIGHT };
      sdl2::render_texture ( sdl2.renderer.get ( ),     //
                             sdl2.fireTexture.get ( ),  //
                             nullptr,                   //
                             &rect );

      sdl2::swap ( sdl2.renderer.get ( ) );

    }  // end of while
  } catch ( const std::exception& e ) {
    std::cerr << e.what ( ) << std::endl;
  }
  return EXIT_SUCCESS;
}
