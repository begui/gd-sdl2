cmake_minimum_required(VERSION 3.13.0)
set( CMAKE_EXPORT_COMPILE_COMMANDS ON ) #For Vim

set (APPNAME "GD_SDL2")

# NOTE we need to verify only one toolchain is selected  ( -DGD_SDL2_BUILD_OD={GCW0,RG350} )
list(APPEND OD_GCW0_TOOLCHAIN GCW0 RG350)
if ( DEFINED ${APPNAME}_BUILD_OD  AND  NOT ${${APPNAME}_BUILD_OD} STREQUAL "" )
  if( ${${APPNAME}_BUILD_OD} IN_LIST OD_GCW0_TOOLCHAIN )
    message (STATUS "Building for ${${APPNAME}_BUILD_OD}")
    include(/opt/gcw0-toolchain/usr/share/buildroot/toolchainfile.cmake)
  else()
    message (FATAL_ERROR "Device ${${APPNAME}_BUILD_OD} not found. List of supported devices ${OD_GCW0_TOOLCHAIN}")
  endif()
endif()

#NOTE: I had to define project down here because doing this then selecting a toolchain will complain about overriding cached values. 
project(${APPNAME} VERSION 2021.1.0 DESCRIPTION "GD SDL2 Helper Library" LANGUAGES CXX)

add_library(${PROJECT_NAME} INTERFACE)

if(NOT DEFINED EMSCRIPTEN )
  
  find_package(PkgConfig REQUIRED)
  pkg_search_module(SDL2 REQUIRED sdl2 )
  list(APPEND ${PROJECT_NAME}_LIBRARIES ${SDL2_LIBRARIES})
  list(APPEND ${PROJECT_NAME}_INCLUDE_DIRS ${SDL2_INCLUDE_DIRS})
  if(USE_SDL2_IMAGE)
    message(STATUS "Enabling SDL2_image")
    pkg_search_module(SDL2_image REQUIRED SDL2_image)
    list(APPEND ${PROJECT_NAME}_LIBRARIES ${SDL2_image_LIBRARIES})
    target_compile_definitions(${PROJECT_NAME} INTERFACE USE_SDL2_IMAGE=1)
  endif()
  if(USE_SDL2_TTF)
    message(STATUS "Enabling SDL2_ttf")
    pkg_search_module(SDL2_ttf REQUIRED SDL2_ttf)
    list(APPEND ${PROJECT_NAME}_LIBRARIES ${SDL2_ttf_LIBRARIES})
    target_compile_definitions(${PROJECT_NAME} INTERFACE USE_SDL2_TTF=1)
  endif()
  if(USE_SDL2_MIXER)
    message(STATUS "Enabling SDL2_mixer")
    pkg_search_module(SDL2_mixer REQUIRED SDL2_mixer)
    list(APPEND ${PROJECT_NAME}_LIBRARIES ${SDL2_mixer_LIBRARIES})
    target_compile_definitions(${PROJECT_NAME} INTERFACE USE_SDL2_MIXER=1)
  endif()


if ( DEFINED ${APPNAME}_BUILD_OD  AND  NOT ${${APPNAME}_BUILD_OD} STREQUAL "" )
  if(${${APPNAME}_BUILD_OD} STREQUAL "GCW0") 
    message (STATUS "Building for ${${APPNAME}_BUILD_OD}")
    target_compile_definitions(${PROJECT_NAME} INTERFACE BUILD_GCW0=1)
  elseif(${${APPNAME}_BUILD_OD} STREQUAL "RG350") 
    message (STATUS "Building for ${${APPNAME}_BUILD_OD}")
    target_compile_definitions(${PROJECT_NAME} INTERFACE BUILD_RG350=1)
  endif()
endif()

endif()

target_include_directories( ${PROJECT_NAME}
  INTERFACE
  $<BUILD_INTERFACE:${${PROJECT_NAME}_BINARY_DIR}/include>
  $<BUILD_INTERFACE:${${PROJECT_NAME}_SOURCE_DIR}/include>
  ${${PROJECT_NAME}_INCLUDE_DIRS}
  )

target_link_libraries(${PROJECT_NAME}
  INTERFACE
  ${${PROJECT_NAME}_LIBRARIES}
  )

target_compile_features(${PROJECT_NAME}
  INTERFACE
  cxx_std_17
  )

if(${PROJECT_NAME}_BUILD_SANDBOX)
  add_subdirectory(sandbox/hello)
  add_subdirectory(sandbox/doomfire)
  add_subdirectory(sandbox/snake)
  add_subdirectory(sandbox/starfield)
endif()

