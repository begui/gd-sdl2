#pragma once

#include <SDL2/SDL.h>

#if USE_SDL2_TTF
#include <SDL2/SDL_ttf.h>
#endif

#if USE_SDL2_MIXER
#include <SDL2/SDL_mixer.h>
#endif

#if USE_SDL2_IMAGE
#include <SDL2/SDL_image.h>
#endif

#ifdef None
#undef None
#endif

#include <memory>
#include <ostream>
#include <string>
#include <type_traits>
#include <typeinfo>
#include <vector>
#include <optional>

inline std::ostream &operator<< ( std::ostream &os, const SDL_Rect &rec ) {  //
  return os << rec.x << " " << rec.y << " " << rec.w << " " << rec.h;
}
inline std::ostream &operator<< ( std::ostream &os, const SDL_Point &point ) {  //
  return os << point.x << " " << point.y;
}
inline std::ostream &operator<< ( std::ostream &os, const SDL_Color &color ) {  //
  return os << ( int ) color.r << " " << ( int ) color.g << " " << ( int ) color.b << " " << ( int ) color.a;
}
namespace sdl2 {

template < typename T >
struct Point : public std::conditional< std::is_floating_point< T >::value, SDL_FPoint, SDL_Point >::type {
  static_assert ( std::is_same< int32_t, T >::value || std::is_same< float, T >::value,
                  "T must inherit from int or float " );

 public:
  Point ( T xx = 0, T yy = 0 )
      : std::conditional< std::is_floating_point< T >::value, SDL_FPoint, SDL_Point >::type{ xx, yy } {}
  constexpr bool operator== ( const Point &other ) const {
    return this->x == other.x && this->y == other.y;
  }
  constexpr bool operator!= ( const Point &other ) const {
    return !( *this == other );
  }
  constexpr bool operator< ( const Point &other ) const {
     return ( ( this->x < other.x ) || ( ( this->x == other.x ) && ( this->y < other.y ) ) );
  }
  void operator*= ( const Point &other ) {
    this->x *= other.x;
    this->y *= other.y;
  }
  void operator/= ( const Point &other ){
    this->x /= other.x;
    this->y /= other.y;
  }
};

template < typename T >
struct Rect : public std::conditional< std::is_floating_point< T >::value, SDL_FRect, SDL_Rect >::type {
  static_assert ( std::is_same< int32_t, T >::value || std::is_same< float, T >::value,
                  "T must inherit from int or float " );

 public:
  Rect ( T xx = 0, T yy = 0, T ww = 0, T hh = 0 )
      : std::conditional< std::is_floating_point< T >::value, SDL_FRect, SDL_Rect >::type{ xx, yy, ww, hh } {}

 public:
  constexpr bool operator== ( const Rect< T > &other ) const {
    return this->x == other.x && this->y == other.y && this->w == other.w && this->h == other.h;
  }
  constexpr bool operator!= ( const Rect< T > &other ) const {
    return !( *this == other );
  }
};

using Pointi32_t       = Point< int32_t >;
using Pointi32Opt_t    = std::optional< Pointi32_t >;
using Pointf_t         = Point< float >;
using PointfOpt_t      = std::optional< Pointf_t >;
using Recti32_t        = Rect< int32_t >;
using Recti32Opt_t     = std::optional< Recti32_t >;
using Rectf_t          = Rect< float >;
using RectfOpt_t       = std::optional< Rectf_t >;

namespace log {
enum class CategoryType
{
  Application = SDL_LOG_CATEGORY_APPLICATION,
  Error       = SDL_LOG_CATEGORY_ERROR,
  Assert      = SDL_LOG_CATEGORY_ASSERT,
  System      = SDL_LOG_CATEGORY_SYSTEM,
  Audio       = SDL_LOG_CATEGORY_AUDIO,
  Video       = SDL_LOG_CATEGORY_VIDEO,
  Render      = SDL_LOG_CATEGORY_RENDER,
  Input       = SDL_LOG_CATEGORY_INPUT,
  Test        = SDL_LOG_CATEGORY_TEST,
  Custom      = SDL_LOG_CATEGORY_CUSTOM
};

enum class PriorityType : std::underlying_type_t< SDL_LogPriority >
{
  Verbose     = SDL_LOG_PRIORITY_VERBOSE,
  Debug       = SDL_LOG_PRIORITY_DEBUG,
  Information = SDL_LOG_PRIORITY_INFO,
  Warning     = SDL_LOG_PRIORITY_WARN,
  Error       = SDL_LOG_PRIORITY_ERROR,
  Critical    = SDL_LOG_PRIORITY_CRITICAL
};

inline void set_priority ( const CategoryType cat, const PriorityType pri ) noexcept {
  SDL_LogSetPriority ( static_cast< std::underlying_type_t< CategoryType > > ( cat ),
                       static_cast< SDL_LogPriority > ( pri ) );
}
inline PriorityType get_priority ( const CategoryType cat ) noexcept {
  return static_cast< PriorityType > (
      SDL_LogGetPriority ( static_cast< std::underlying_type_t< CategoryType > > ( cat ) ) );
}
inline void set_all_priorities ( const PriorityType pri ) noexcept {
  SDL_LogSetAllPriority ( static_cast< SDL_LogPriority > ( pri ) );
}
inline void reset_all_priorities ( ) noexcept { SDL_LogResetPriorities ( ); }

template < typename... Args >
inline void log ( const char *fmt, Args &&...args ) noexcept {
  SDL_Log ( fmt, std::forward< Args > ( args )... );
}
template < typename... Args >
inline void verbose ( const CategoryType cat, const char *fmt, Args &&...args ) noexcept {
  SDL_LogVerbose ( static_cast< std::underlying_type_t< CategoryType > > ( cat ), fmt,
                   std::forward< Args > ( args )... );
}
template < typename... Args >
inline void debug ( const CategoryType cat, const char *fmt, Args &&...args ) noexcept {
  SDL_LogDebug ( static_cast< std::underlying_type_t< CategoryType > > ( cat ), fmt, std::forward< Args > ( args )... );
}
template < typename... Args >
inline void info ( const CategoryType cat, const char *fmt, Args &&...args ) noexcept {
  SDL_LogInfo ( static_cast< std::underlying_type_t< CategoryType > > ( cat ), fmt, std::forward< Args > ( args )... );
}
template < typename... Args >
inline void warn ( const CategoryType cat, const char *fmt, Args &&...args ) noexcept {
  SDL_LogWarn ( static_cast< std::underlying_type_t< CategoryType > > ( cat ), fmt, std::forward< Args > ( args )... );
}
template < typename... Args >
inline void error ( const CategoryType cat, const char *fmt, Args &&...args ) noexcept {
  SDL_LogError ( static_cast< std::underlying_type_t< CategoryType > > ( cat ), fmt, std::forward< Args > ( args )... );
}
template < typename... Args >
inline void critical ( const CategoryType cat, const char *fmt, Args &&...args ) noexcept {
  SDL_LogCritical ( static_cast< std::underlying_type_t< CategoryType > > ( cat ), fmt,
                    std::forward< Args > ( args )... );
}
inline void log ( const char *str ) noexcept { log ( "%s", str ); }
inline void verbose ( const CategoryType cat, const char *str ) noexcept { verbose ( cat, "%s", str ); }
inline void debug ( const CategoryType cat, const char *str ) noexcept { debug ( cat, "%s", str ); }
inline void info ( const CategoryType cat, const char *str ) noexcept { info ( cat, "%s", str ); }
inline void warn ( const CategoryType cat, const char *str ) noexcept { warn ( cat, "%s", str ); }
inline void error ( const CategoryType cat, const char *str ) noexcept { error ( cat, "%s\n", str ); }
inline void critical ( const CategoryType cat, const char *str ) noexcept { critical ( cat, "%s", str ); }

inline bool is_debug ( ) { return log::PriorityType::Debug == log::get_priority ( log::CategoryType::Application ); }

}  // namespace log

namespace internal {
template < typename T >
constexpr auto to_type ( T t ) {
  return static_cast< typename std::underlying_type< T >::type > ( t );
}

template < typename TO, typename T >
constexpr TO to_type ( T t ) {
  return static_cast< TO > ( to_type ( t ) );
}

template < typename H, typename... T >
using are_same = std::conjunction< std::is_same< H, T >... >;

template < typename H, typename... T, typename = std::enable_if_t< are_same< H, T... >::value, void > >
auto enum_class_or ( H head, T... tail ) {
  using type = typename std::underlying_type< H >::type;
  return ( static_cast< type > ( head ) | ... | static_cast< type > ( tail ) );
}

template < typename T, typename Ptr, void del_func ( Ptr * ) >
struct SDLDeleter {
  void operator( ) ( Ptr *ptr ) const noexcept {
    if ( ptr ) {
      if ( log::is_debug ( ) ) {
        std::string buf ( "Freeing " );
        buf.append ( typeid ( ptr ).name ( ) );
        log::debug ( log::CategoryType::Application, buf.c_str ( ) );
      }
      del_func ( ptr );
    }
  }
};

template < typename T, void del_func ( T * ) >
using sdl2_ptr = std::unique_ptr< T, SDLDeleter< T, T, del_func > >;

}  // namespace internal

enum class SubsystemType
{
  //
  Core = 1 << 0,
  //
  Ttf = 1 << 1,
  //
  Image = 1 << 2,
  //
  Audio = 1 << 3
};

enum class InitType : uint32_t
{
  //
  Timer = SDL_INIT_TIMER,
  //
  Audio = SDL_INIT_AUDIO,
  //
  Video = SDL_INIT_VIDEO,
  //
  Joystick = SDL_INIT_JOYSTICK,
  //
  Haptic = SDL_INIT_HAPTIC,
  //
  Controller = SDL_INIT_GAMECONTROLLER,
  //
  Events = SDL_INIT_EVENTS,
  //
  Everything = SDL_INIT_EVERYTHING
};

enum class WindowType
{
  // test
  None = 0x00000000,
  // full screen (0x00000001)
  Fullscreen = SDL_WINDOW_FULLSCREEN,
  // opengl window (0x00000002)
  Opengl = SDL_WINDOW_OPENGL,
  // window is visible
  Shown = SDL_WINDOW_SHOWN,
  // window is not visible
  Hidden = SDL_WINDOW_HIDDEN,
  // no decorations
  Borderless = SDL_WINDOW_BORDERLESS,
  // allows to be resizable
  Resizeable = SDL_WINDOW_RESIZABLE,
  // window is minimized
  Minimized = SDL_WINDOW_MINIMIZED,
  // window is maximized
  Maximized = SDL_WINDOW_MAXIMIZED,
  // window has grab input focus
  Inputgrab = SDL_WINDOW_INPUT_GRABBED,
  // window has input focus
  IInputFocus = SDL_WINDOW_INPUT_FOCUS,
  // window has mouse focus
  MouseFocus = SDL_WINDOW_MOUSE_FOCUS,
  // full screen at current desktop rez
  FullscreenDesktop = SDL_WINDOW_FULLSCREEN_DESKTOP,
  // high dpi if supported
  Highdpi = SDL_WINDOW_ALLOW_HIGHDPI
};

enum class WindowStateType
{
  None,  //
  Fullscreen,
  FullscreenDesktop,
  Minimize,
  Maximize,
  Restore,
  Hide,
  Show,
  Raise
};

enum class RendererType : uint32_t
{
  // software acceleration
  Software = SDL_RENDERER_SOFTWARE,
  // hardware acceleration
  Accelerated = SDL_RENDERER_ACCELERATED,
  // allows vsync
  Vsync = SDL_RENDERER_PRESENTVSYNC,
  // supports rendering to a texture
  Texture = SDL_RENDERER_TARGETTEXTURE
};

enum class TextureAccessType
{
  Static    = SDL_TEXTUREACCESS_STATIC,
  Streaming = SDL_TEXTUREACCESS_STREAMING,
  Target    = SDL_TEXTUREACCESS_TARGET
};

enum class BlendmodeType
{
  // None:
  // dstRGBA = srcRGBA
  None = SDL_BLENDMODE_NONE,
  // Alpha:
  // dstRGB = (srcRGB * srcA) + (dstRGB * (1-srcA))
  // dstA = srcA + (dstA * (1-srcA))
  Blend = SDL_BLENDMODE_BLEND,
  // Additive blending:
  // dstRGB = (srcRGB * srcA) + dstRGB
  // dstA = dstA
  Add = SDL_BLENDMODE_ADD,
  // Color modulate:
  // dstRGB = srcRGB * dstRGB
  // dstA = dstA
  Mod = SDL_BLENDMODE_MOD
};

enum class FlipType
{
  None       = SDL_FLIP_NONE,  //
  Horizontal = SDL_FLIP_HORIZONTAL,
  Vertical   = SDL_FLIP_VERTICAL
};

inline auto timer ( const bool highprecision ) noexcept {
  if ( highprecision ) {
    static auto period = 1.0 / static_cast< double > ( SDL_GetPerformanceFrequency ( ) );
    return static_cast< double > ( SDL_GetPerformanceCounter ( ) ) * period;
  }
  return SDL_GetTicks ( ) / 1000.0;
}

using WindowUniqPtr = internal::sdl2_ptr< SDL_Window, SDL_DestroyWindow >;
using WindowShrdPtr = std::shared_ptr< SDL_Window >;
using WindowWeakPtr = std::weak_ptr< SDL_Window >;

using RendererUniqPtr = internal::sdl2_ptr< SDL_Renderer, SDL_DestroyRenderer >;
using RendererShrdPtr = std::shared_ptr< SDL_Renderer >;
using RendererWeakPtr = std::weak_ptr< SDL_Renderer >;

using SurfaceUniqPtr = internal::sdl2_ptr< SDL_Surface, SDL_FreeSurface >;
using SurfaceShrdPtr = std::shared_ptr< SDL_Surface >;
using SurfaceWeakPtr = std::weak_ptr< SDL_Surface >;

using TextureUniqPtr = internal::sdl2_ptr< SDL_Texture, SDL_DestroyTexture >;
using TextureShrdPtr = std::shared_ptr< SDL_Texture >;
using TextureWeakPtr = std::weak_ptr< SDL_Texture >;

using PixelFormatUniqPtr = internal::sdl2_ptr< SDL_PixelFormat, SDL_FreeFormat >;

struct RWDeleter {
  void operator( ) ( SDL_RWops *ptr ) const noexcept {
    if ( ptr )
      SDL_RWclose ( ptr );
  }
};
using RWopsUniqPtr = std::unique_ptr< SDL_RWops, RWDeleter >;

#if USE_SDL2_TTF
using TTFFontUniqPtr = internal::sdl2_ptr< TTF_Font, TTF_CloseFont >;
using TTFFontShrdPtr = std::shared_ptr< TTF_Font >;
using TTFFontWeakPtr = std::weak_ptr< TTF_Font >;

#endif

#if USE_SDL2_MIXER
using MixChunkUniqPtr = internal::sdl2_ptr< Mix_Chunk, Mix_FreeChunk >;
using MixChunkShrdPtr = std::shared_ptr< Mix_Chunk >;
using MixChunkWeakPtr = std::weak_ptr< Mix_Chunk >;

using MixMusicUniqPtr = internal::sdl2_ptr< Mix_Music, Mix_FreeMusic >;
using MixMusicShrdPtr = std::shared_ptr< Mix_Music >;
using MixMusicWeakPtr = std::weak_ptr< Mix_Music >;

#endif
template < typename... T, typename = std::enable_if_t< internal::are_same< sdl2::InitType, T... >::value, void > >
inline auto isInit ( InitType head, T... tail ) noexcept {
  const uint32_t flags = internal::enum_class_or ( head, std::forward ( tail )... );
  return ( ::SDL_WasInit ( flags ) & flags ) != 0u;
}
inline bool isJoyStickEnabled ( ) noexcept { return isInit ( InitType::Haptic ); }
inline bool isHapticEnabled ( ) noexcept { return isInit ( InitType::Haptic ) != 0u; }
inline bool isGameControllerEnabled ( ) noexcept { return isInit ( InitType::Controller ) != 0u; }
inline bool isEventsEnabled ( ) noexcept { return isInit ( InitType::Events ); }
inline bool isAudioEnabled ( ) noexcept { return isInit ( InitType::Audio ); }
inline bool isTimerEnabled ( ) noexcept { return isInit ( InitType::Timer ); }
inline bool isVideoEnabled ( ) noexcept { return isInit ( InitType::Video ); }
inline bool isEverythingEnabled ( ) noexcept { return isInit ( InitType::Everything ); }
#if USE_SDL2_TTF
inline bool isTTFEnabled ( ) noexcept { return ::TTF_WasInit ( ) != 0 ? true : false; }
#endif
//
// Initializes sdl, don't forget to call SDL_Quit
//
inline void quit ( ) noexcept {
  log::debug ( log::CategoryType::Application, "Quitting SDL\n" );
  ::SDL_Quit ( );
}

template < typename... T, typename = std::enable_if_t< internal::are_same< sdl2::InitType, T... >::value, void > >
inline auto init ( InitType head = InitType::Everything, T... tail ) noexcept {
#ifdef __EMSCRIPTEN__
  ////Note: There must be another flag that is causing it to throw an error
  // flags &= ~( internal::enum_class_or ( InitType::Haptic ) );
  //// Forcing the list of flags that work with EmScripten
  const uint32_t flags = internal::enum_class_or ( InitType::Video, InitType::Timer, InitType::Events,
                                                        InitType::Controller, InitType::Joystick, InitType::Audio );
#else
  const uint32_t flags = internal::enum_class_or ( head, std::forward< sdl2::InitType > ( tail )... );
#endif
  const auto rtn = ::SDL_Init ( flags );
  if ( rtn != 0 ) {
    log::error ( log::CategoryType::Application, "Issue initializing SDL: %s\n", SDL_GetError ( ) );
  }
  std::atexit ( quit );
  return rtn;
}

#if USE_SDL2_IMAGE
namespace image {
enum class ImageType
{
  Png = IMG_INIT_PNG,  //
  Jpg = IMG_INIT_JPG,
  Tif = IMG_INIT_TIF
};
inline void quit ( ) noexcept {
  log::debug ( log::CategoryType::Application, "Quitting Image\n" );
  ::IMG_Quit ( );
}

template < typename... T,
           typename = std::enable_if_t< internal::are_same< sdl2::image::ImageType, T... >::value, void > >
inline auto init ( image::ImageType head = image::ImageType::Png, T... tail ) noexcept {
  const auto flags        = internal::enum_class_or ( head, std::forward< sdl2::image::ImageType > ( tail )... );
  const auto imgFlagsInit = IMG_Init ( flags );
  const auto rtn          = ( imgFlagsInit & flags ) != flags;
  if ( rtn != 0 ) {
    log::error ( log::CategoryType::Application, "Issue initializing SDL: %s\n", IMG_GetError ( ) );
  }
  std::atexit ( quit );
  return rtn;
}
}  // namespace image
#endif

inline auto rw_FromFile ( const char *file, const char *mode ) noexcept {
  auto ptr = ::SDL_RWFromFile ( file, mode );
  if ( !ptr ) {
    log::error ( log::CategoryType::Application, "Failed to create SDL_RWFromFile" );
  }
  return RWopsUniqPtr ( ptr );
}

inline auto rw_FromConstMem ( const uint8_t *buffer, const std::size_t size ) noexcept {
  auto ptr = ::SDL_RWFromConstMem ( buffer, static_cast< int > ( size ) );
  if ( !ptr ) {
    log::error ( log::CategoryType::Application, "Failed to create SDL_RWFromConstMem" );
  }
  return RWopsUniqPtr ( ptr );
}

inline auto rw_FromMem ( uint8_t *buffer, const std::size_t size ) noexcept {  //
  auto ptr = ::SDL_RWFromMem ( buffer, static_cast< int > ( size ) );
  if ( !ptr ) {
    log::error ( log::CategoryType::Application, "Failed to create SDL_RWFromMem" );
  }
  return RWopsUniqPtr ( ptr );
}

inline const std::string base_path ( ) noexcept {
  using sdl_path = std::unique_ptr< char, decltype ( &SDL_free ) >;
  auto cstr      = sdl_path ( ::SDL_GetBasePath ( ), ::SDL_free );
  if ( cstr ) {
    const std::string sstr{ cstr.get ( ) };
    return sstr;
  }
  return { };
}
inline const std::string pref_path ( const char *org, const char *app ) noexcept {
  using sdl_path = std::unique_ptr< char, decltype ( &SDL_free ) >;
  auto cstr      = sdl_path ( ::SDL_GetPrefPath ( org, app ), ::SDL_free );
  if ( cstr ) {
    const std::string sstr{ cstr.get ( ) };
    return sstr;
  }
  return { };
}

namespace system {
namespace cpu {
inline auto count ( ) noexcept { return ::SDL_GetCPUCount ( ); }
inline auto has_3dnow ( ) noexcept { return ::SDL_Has3DNow ( ) == SDL_TRUE; }
inline auto has_altivec ( ) noexcept { return ::SDL_HasAltiVec ( ) == SDL_TRUE; }
inline auto has_avx ( ) noexcept { return ::SDL_HasAVX ( ) == SDL_TRUE; }
inline auto has_avx2 ( ) noexcept { return ::SDL_HasAVX2 ( ) == SDL_TRUE; }
inline auto has_mmx ( ) noexcept { return ::SDL_HasMMX ( ) == SDL_TRUE; }
inline auto has_rdtsc ( ) noexcept { return ::SDL_HasRDTSC ( ) == SDL_TRUE; }
inline auto has_sse ( ) noexcept { return ::SDL_HasSSE ( ) == SDL_TRUE; }
inline auto has_sse2 ( ) noexcept { return ::SDL_HasSSE2 ( ) == SDL_TRUE; }
inline auto has_sse3 ( ) noexcept { return ::SDL_HasSSE3 ( ) == SDL_TRUE; }
inline auto has_sse41 ( ) noexcept { return ::SDL_HasSSE41 ( ) == SDL_TRUE; }
inline auto has_sse42 ( ) noexcept { return ::SDL_HasSSE42 ( ) == SDL_TRUE; }
}  // namespace cpu

inline auto platform ( ) noexcept { return ::SDL_GetPlatform ( ); }
inline auto ram ( ) noexcept { return ::SDL_GetSystemRAM ( ); }
inline auto video_driver ( int index = -1 ) noexcept {
  const auto numVideoDrivers = ::SDL_GetNumVideoDrivers ( );
  if ( index < 0 || index > numVideoDrivers ) {
    return ::SDL_GetCurrentVideoDriver ( );
  }
  return ::SDL_GetVideoDriver ( index );
}
}  // namespace system

//
//
//
inline auto get_version ( SubsystemType subSystem ) noexcept {
  SDL_version version{ 0, 0, 0 };
  switch ( subSystem ) {
    case SubsystemType::Core:
      SDL_VERSION ( &version );
      break;
    case SubsystemType::Image:
#if USE_SDL2_IMAGE
      SDL_IMAGE_VERSION ( &version );
#endif
      break;
    case SubsystemType::Ttf:
#if USE_SDL2_TTF
      SDL_TTF_VERSION ( &version );
#endif
      break;
    case SubsystemType::Audio:
#if USE_SDL2_MIXER
      SDL_MIXER_VERSION ( &version );
#endif
      break;
  }
  return version;
}

// Blend Mode
inline auto get_blend_mode ( SDL_Renderer *ptr ) noexcept {
  SDL_BlendMode blendmode = SDL_BLENDMODE_NONE;
  if ( ::SDL_GetRenderDrawBlendMode ( ptr, &blendmode ) != 0 ) {
    log::error ( log::CategoryType::Application, "Failed to get Renderer Blend mode state." );
  }
  return static_cast< BlendmodeType > ( blendmode );
}
inline auto get_blend_mode ( SDL_Texture *ptr ) noexcept {
  SDL_BlendMode blendmode = SDL_BLENDMODE_NONE;
  if ( ::SDL_GetTextureBlendMode ( ptr, &blendmode ) != 0 ) {
    log::error ( log::CategoryType::Application, "Failed to get Texture Blend mode state." );
  }
  return static_cast< BlendmodeType > ( blendmode );
}
inline auto get_blend_mode ( SDL_Surface *ptr ) noexcept {
  SDL_BlendMode blendmode = SDL_BLENDMODE_NONE;
  if ( ::SDL_GetSurfaceBlendMode ( ptr, &blendmode ) != 0 ) {
    log::error ( log::CategoryType::Application, "Failed to get Surface Blend mode state." );
  }
  return static_cast< BlendmodeType > ( blendmode );
}
inline void set_blend_mode ( SDL_Renderer *ptr, const BlendmodeType mode = BlendmodeType::None ) noexcept {
  if ( ::SDL_SetRenderDrawBlendMode ( ptr, internal::to_type< SDL_BlendMode > ( mode ) ) != 0 ) {
    log::error ( log::CategoryType::Application, "Failed to set Renderer Blend mode state." );
  }
}
inline void set_blend_mode ( SDL_Texture *ptr, const BlendmodeType mode = BlendmodeType::None ) noexcept {
  if ( ::SDL_SetTextureBlendMode ( ptr, internal::to_type< SDL_BlendMode > ( mode ) ) != 0 ) {
    log::error ( log::CategoryType::Application, "Failed to set Texture Blend mode state." );
  }
}
inline void set_blend_mode ( SDL_Surface *ptr, BlendmodeType mode = BlendmodeType::None ) noexcept {
  if ( ::SDL_SetSurfaceBlendMode ( ptr, internal::to_type< SDL_BlendMode > ( mode ) ) != 0 ) {
    log::error ( log::CategoryType::Application, "Failed to set Surface Blend mode state." );
  }
}
// Alpha Mod
inline uint8_t get_alpha_mod ( SDL_Texture *ptr ) noexcept {
  uint8_t alpha{ 255 };
  if ( ::SDL_GetTextureAlphaMod ( ptr, &alpha ) != 0 ) {
    log::error ( log::CategoryType::Application, "Failed to get Texture Alpha Mod state." );
  }
  return alpha;
}
inline uint8_t get_alpha_mod ( SDL_Surface *ptr ) noexcept {
  uint8_t alpha{ 255 };
  if ( ::SDL_GetSurfaceAlphaMod ( ptr, &alpha ) != 0 ) {
    log::error ( log::CategoryType::Application, "Failed to get Surface Alpha Mod state." );
  }
  return alpha;
}
inline void set_alpha_mod ( SDL_Texture *ptr, const uint8_t alpha = SDL_ALPHA_OPAQUE ) noexcept {
  ::SDL_SetTextureAlphaMod ( ptr, alpha );
}
inline void set_alpha_mod ( SDL_Surface *ptr, const uint8_t alpha = SDL_ALPHA_OPAQUE ) noexcept {
  ::SDL_SetSurfaceAlphaMod ( ptr, alpha );
}
// Color Mod
inline SDL_Color get_color_mod ( SDL_Texture *ptr ) noexcept {
  uint8_t r, g, b;
  ::SDL_GetTextureColorMod ( ptr, &r, &g, &b );
  return { r, g, b, 0 };
}
inline SDL_Color get_color_mod ( SDL_Surface *ptr ) noexcept {
  uint8_t r, g, b;
  ::SDL_GetSurfaceColorMod ( ptr, &r, &g, &b );
  return { r, g, b, 0 };
}
inline void set_color_mod ( SDL_Texture *ptr, const SDL_Color &color ) noexcept {
  ::SDL_SetTextureColorMod ( ptr, color.r, color.g, color.b );
}
inline void set_color_mod ( SDL_Surface *ptr, const SDL_Color &color ) noexcept {
  ::SDL_SetSurfaceColorMod ( ptr, color.r, color.g, color.b );
}
//
// Window
//
template < typename... T, typename = std::enable_if_t< internal::are_same< WindowType, T... >::value, void > >
WindowUniqPtr create_window ( const int32_t width, int32_t height,
                              const Pointi32_t &position = { SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED },
                              WindowType head           = WindowType::None, T... tail ) noexcept {
  const uint32_t flags = internal::enum_class_or ( head, std::forward< WindowType > ( tail )... );
  auto ptr                  = ::SDL_CreateWindow ( "", position.x, position.y, width, height, flags );
  if ( !ptr ) {
    log::error ( log::CategoryType::Application, "Failed to create window." );
    return nullptr;
  }
  return WindowUniqPtr ( ptr );
}
//
// Renderer
//
template < typename... T, typename = std::enable_if_t< internal::are_same< RendererType, T... >::value, void > >
inline RendererUniqPtr create_renderer ( SDL_Window *window, const BlendmodeType blendmode = BlendmodeType::None, RendererType t0= RendererType::Vsync, RendererType t1 = RendererType::Accelerated,
                                         T... tN  ) noexcept {
  if ( window ) {
    const uint32_t flags = internal::enum_class_or ( t0, t1, std::forward< RendererType > ( tN )... );
    auto renderer             = ::SDL_CreateRenderer ( window, -1, flags );
    if(!renderer) {
      log::error ( log::CategoryType::Application, "Failed to create Renderer." );
      return nullptr;
    }
    set_blend_mode ( renderer, blendmode );
    return RendererUniqPtr ( renderer );
  }
  return nullptr;
}

inline auto get_window_size ( SDL_Window *window ) noexcept {
  int width, height;
  ::SDL_GetWindowSize ( window, &width, &height );
  return std::make_tuple ( width, height );
}
inline auto get_window_id ( SDL_Window *window ) noexcept { return ::SDL_GetWindowID ( window ); }
inline auto get_window_position ( SDL_Window *window ) {
  int width, height;
  ::SDL_GetWindowPosition ( window, &width, &height );
  return std::make_tuple ( width, height );
}
inline auto get_window_pixelformat ( SDL_Window *window ) noexcept { return ::SDL_GetWindowPixelFormat ( window ); }

inline auto create_pixelformat ( SDL_Window *window ) noexcept {
  return PixelFormatUniqPtr ( ::SDL_AllocFormat ( ::SDL_GetWindowPixelFormat ( window ) ) );
}

inline void set_window_position ( SDL_Window *window, const int32_t x, const int32_t y ) noexcept {
  ::SDL_SetWindowPosition ( window, x, y );
}
inline auto get_window_aspect_ratio ( SDL_Window *window ) {
  auto size = get_window_size ( window );
  return std::get< 0 > ( size ) / std::get< 1 > ( size );
}
inline void set_window_title ( SDL_Window *window, const char *text ) noexcept {
  ::SDL_SetWindowTitle ( window, text );
}

inline void set_window_size ( SDL_Window *window, const int32_t width, const int32_t height ) noexcept {
  ::SDL_SetWindowSize ( window, width, height );
}

inline auto get_window_display_index ( SDL_Window *window ) noexcept { return ::SDL_GetWindowDisplayIndex ( window ); }

inline auto get_window_refresh_rate ( SDL_Window *window, const int defaultRefershRate = 60 ) noexcept {
  SDL_DisplayMode mode;
  if ( ::SDL_GetDesktopDisplayMode ( get_window_display_index ( window ), &mode ) != 0 || mode.refresh_rate == 0 ) {
    return defaultRefershRate;
  }
  return mode.refresh_rate;
}

inline auto get_window_refresh_rate_current ( SDL_Window *window, const int defaultRefershRate = 60 ) noexcept {
  SDL_DisplayMode mode;
  if ( ::SDL_GetCurrentDisplayMode ( get_window_display_index ( window ), &mode ) != 0 || mode.refresh_rate == 0 ) {
    return defaultRefershRate;
  }
  return mode.refresh_rate;
}

inline void set_window_opacity ( SDL_Window *window, const float opacity ) noexcept {
  ::SDL_SetWindowOpacity ( window, opacity );
}
inline auto get_window_opacity ( SDL_Window *window ) noexcept {
  float opacity{ -1 };
  ::SDL_GetWindowOpacity ( window, &opacity );
  return opacity;
}

inline auto set_window_state ( SDL_Window *window, const WindowStateType state ) noexcept {
  switch ( state ) {
    case WindowStateType::None:
      ::SDL_SetWindowFullscreen ( window, 0 );
      break;
    case WindowStateType::Fullscreen:
      ::SDL_SetWindowFullscreen ( window, SDL_WINDOW_FULLSCREEN );
      break;
    case WindowStateType::FullscreenDesktop:
      ::SDL_SetWindowFullscreen ( window, SDL_WINDOW_FULLSCREEN_DESKTOP );
      break;
    case WindowStateType::Show:
      ::SDL_ShowWindow ( window );
      break;
    case WindowStateType::Hide:
      ::SDL_HideWindow ( window );
      break;
    case WindowStateType::Maximize:
      ::SDL_MaximizeWindow ( window );
      break;
    case WindowStateType::Minimize:
      ::SDL_MinimizeWindow ( window );
      break;
    case WindowStateType::Restore:
      ::SDL_RestoreWindow ( window );
      break;
    case WindowStateType::Raise:
      ::SDL_RaiseWindow ( window );
      break;
  }
}
inline void swap ( SDL_Window *window ) noexcept { ::SDL_GL_SwapWindow ( window ); }
inline void swap ( SDL_Renderer *renderer ) noexcept { ::SDL_RenderPresent ( renderer ); }

inline auto get_renderer_info ( SDL_Renderer *renderer ) noexcept {
  SDL_RendererInfo info;
  ::SDL_GetRendererInfo ( renderer, &info );
  return std::make_tuple ( info.name, info.max_texture_width, info.max_texture_height );
}
//
//
//
inline bool keyboard ( const SDL_Scancode scancode ) noexcept {
  static int numkeys          = 0;
  static const uint8_t *state = ::SDL_GetKeyboardState ( &numkeys );
  return state[ scancode ] != 0;
}
enum class MouseStateType
{
  // Coordinates based on the resolution of the window and not its logical size
  None,
  // The direction of the mouse (pos / neg numbers)
  Relative,
  // Global mouse coordinate based on users screen resolution
  Global
};
inline auto mouse_position ( const MouseStateType type = MouseStateType::None ) noexcept {
  int x{ 0 };
  int y{ 0 };

  switch ( type ) {
    case MouseStateType::None:
      ::SDL_GetMouseState ( &x, &y );
      break;
    case MouseStateType::Relative:
      ::SDL_GetRelativeMouseState ( &x, &y );
      break;
    case MouseStateType::Global:
      ::SDL_GetGlobalMouseState ( &x, &y );
      break;
    default:
      break;
  }
  return std::make_tuple ( x, y );
}

inline auto mouse_logical_position(SDL_Renderer * renderer) noexcept {
  auto [wX,wY] = mouse_position();
  float logicalX{0.f};
  float logicalY{0.f};
  SDL_RenderWindowToLogical(renderer, wX, wY, &logicalX, &logicalY);
  return std::make_tuple(logicalX,logicalY);
}

//
// While in Relative Mode, the cursor is hidden.
//
inline void enable_mouse_relative ( const bool enable ) noexcept {
  if ( enable && ::SDL_GetRelativeMouseMode ( ) == SDL_FALSE ) {
    ::SDL_SetRelativeMouseMode ( SDL_TRUE );
  } else if ( !enable && ::SDL_GetRelativeMouseMode ( ) == SDL_TRUE ) {
    ::SDL_SetRelativeMouseMode ( SDL_FALSE );
  } else {
    // Do nothing
  }
}

inline void enable_mouse_grab ( SDL_Window *window, const bool enable ) noexcept {
  if ( enable && ::SDL_GetRelativeMouseMode ( ) == SDL_TRUE ) {
    ::SDL_SetRelativeMouseMode ( SDL_FALSE );
    ::SDL_SetWindowGrab ( window, SDL_FALSE );
  } else {
    ::SDL_SetRelativeMouseMode ( SDL_TRUE );
    ::SDL_SetWindowGrab ( window, SDL_TRUE );
  }
}

inline void enable_mouse_cursor ( const bool enable ) noexcept {
  ::SDL_ShowCursor ( enable ? SDL_ENABLE : SDL_DISABLE );
}

inline void set_transparent_pixel ( SDL_Surface *ptr, const bool enable, const int8_t r = 0,
                                    const int8_t g = 0, const int8_t b = 0 ) noexcept {
  if ( ptr ) {
    if ( ::SDL_SetColorKey ( ptr, enable ? SDL_TRUE : SDL_FALSE, ::SDL_MapRGB ( ptr->format, r, g, b ) ) != 0 ) {
      log::error ( log::CategoryType::Application, "Failed to set color key." );
    }
  } else {
    log::error ( log::CategoryType::Application, "Failed to set color key, Surface is null." );
  }
}
//
// Surface functions
//
template < typename T >
T create_surface ( const int32_t width, const int32_t height, const int32_t depth = 32 ) noexcept;

template <>
inline SDL_Surface *create_surface ( const int32_t width, const int32_t height,
                                     const int32_t depth ) noexcept {
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
  auto rmask = 0xff000000;
  auto gmask = 0x00ff0000;
  auto bmask = 0x0000ff00;
  auto amask = 0x000000ff;
#else
  auto rmask                = 0x000000ff;
  auto gmask                = 0x0000ff00;
  auto bmask                = 0x00ff0000;
  auto amask                = 0xff000000;
#endif

  auto ptr = ::SDL_CreateRGBSurface ( 0, width, height, depth, rmask, gmask, bmask, amask );
  if ( !ptr ) {
    log::error ( log::CategoryType::Application, "Failed to create surface ." );
  }
  return ptr;
}
template <>
inline SurfaceUniqPtr create_surface ( const int32_t width, const int32_t height,
                                       const int32_t depth ) noexcept {
  return SurfaceUniqPtr ( create_surface< SDL_Surface * > ( width, height, depth ) );
}

template <>
inline SurfaceShrdPtr create_surface ( const int32_t width, const int32_t height,
                                       const int32_t depth ) noexcept {
  return SurfaceShrdPtr ( create_surface< SurfaceUniqPtr > ( width, height, depth ) );
}
//
//
template < typename T >
T create_surface ( const int32_t width, const int32_t height, const int32_t depth,
                   const SDL_Color &color ) noexcept;

template <>
inline SDL_Surface *create_surface ( const int32_t width, const int32_t height, const int32_t depth,
                                     const SDL_Color &color ) noexcept {
  auto ptr = create_surface< SDL_Surface * > ( width, height, depth );
  if ( !ptr ) {
    log::error ( log::CategoryType::Application, "Failed to create surface ." );
    return nullptr;
  }
  set_transparent_pixel ( ptr, ptr->format != nullptr, color.r, color.g, color.b );
  return ptr;
}

template <>
inline SurfaceUniqPtr create_surface ( const int32_t width, const int32_t height, const int32_t depth,
                                       const SDL_Color &color ) noexcept {
  return SurfaceUniqPtr ( create_surface< SDL_Surface * > ( width, height, depth, color ) );
}

template <>
inline SurfaceShrdPtr create_surface ( const int32_t width, const int32_t height, const int32_t depth,
                                       const SDL_Color &color ) noexcept {
  return SurfaceShrdPtr ( create_surface< SurfaceUniqPtr > ( width, height, depth, color ) );
}
//
//
template < typename T >
T create_surface ( const char *filename ) noexcept;

template <>
inline SDL_Surface *create_surface ( const char *filename ) noexcept {
#if USE_SDL2_IMAGE
  auto ptr = ::IMG_Load ( filename );
#else
  auto ptr                  = ::SDL_LoadBMP ( filename );
#endif
  if ( !ptr ) {
    log::error ( log::CategoryType::Application, "Failed to create surface ." );
  }
  return ptr;
}
template <>
inline SurfaceUniqPtr create_surface ( const char *filename ) noexcept {
  return SurfaceUniqPtr ( create_surface< SDL_Surface * > ( filename ) );
}
template <>
inline SurfaceShrdPtr create_surface ( const char *filename ) noexcept {
  return SurfaceShrdPtr ( create_surface< SurfaceUniqPtr > ( filename ) );
}
template < typename T >
T create_surface ( const char *filename, const SDL_Color &color ) noexcept;

template <>
inline SDL_Surface *create_surface ( const char *filename, const SDL_Color &color ) noexcept {
  auto ptr = create_surface< SDL_Surface * > ( filename );
  set_transparent_pixel ( ptr, ptr->format != nullptr, color.r, color.g, color.b );
  return ptr;
}
template <>
inline SurfaceUniqPtr create_surface ( const char *filename, const SDL_Color &color ) noexcept {
  return SurfaceUniqPtr ( create_surface< SDL_Surface * > ( filename, color ) );
}
template <>
inline SurfaceShrdPtr create_surface ( const char *filename, const SDL_Color &color ) noexcept {
  return SurfaceShrdPtr ( create_surface< SurfaceUniqPtr > ( filename, color ) );
}
//
inline void render_scale_quality ( const bool useNearest = true ) noexcept {
  if ( ::SDL_SetHint ( SDL_HINT_RENDER_SCALE_QUALITY, useNearest ? "nearest" : "linear" ) < 0 ) {
    log::error ( log::CategoryType::Application, "Failed to set Render Scale Quality" );
  }
}
//
inline auto get_texture_dimension ( SDL_Texture *ptr ) noexcept {
  int w = 0, h = 0;
  if ( ptr && ::SDL_QueryTexture ( ptr, nullptr, nullptr, &w, &h ) != 0 ) {
    log::error ( log::CategoryType::Application, "Failed to query Texture." );
  }
  return std::make_tuple ( w, h );
}
//
//
//
template < typename T >
T surface_to_texture ( SDL_Renderer *renderer, SurfaceUniqPtr &&surface ) noexcept;

template <>
inline SDL_Texture *surface_to_texture ( SDL_Renderer *renderer, SurfaceUniqPtr &&surface ) noexcept {
  return ::SDL_CreateTextureFromSurface ( renderer, surface.get ( ) );
}
template <>
inline TextureUniqPtr surface_to_texture ( SDL_Renderer *renderer, SurfaceUniqPtr &&surface ) noexcept {
  return TextureUniqPtr (
      surface_to_texture< SDL_Texture * > ( renderer, std::forward< SurfaceUniqPtr > ( surface ) ) );
}
template <>
inline TextureShrdPtr surface_to_texture ( SDL_Renderer *renderer, SurfaceUniqPtr &&surface ) noexcept {
  return TextureShrdPtr (
      surface_to_texture< TextureUniqPtr > ( renderer, std::forward< SurfaceUniqPtr > ( surface ) ) );
}
//
template < typename T >
T create_texture ( SDL_Renderer *renderer, const int32_t pixelFormat, const TextureAccessType textureAccess,
                   const int32_t width, const int32_t height ) noexcept;

template <>
inline SDL_Texture *create_texture ( SDL_Renderer *renderer, const int32_t pixelFormat,
                                     const TextureAccessType textureAccess, const int32_t width,
                                     const int32_t height ) noexcept {
  auto ptr = ::SDL_CreateTexture ( renderer, pixelFormat, internal::to_type ( textureAccess ), width, height );
  if ( !ptr ) {
    log::error ( log::CategoryType::Application, "Failed to create Texture." );
  }
  return ptr;
}
template <>
inline TextureUniqPtr create_texture ( SDL_Renderer *renderer, const int32_t pixelFormat,
                                       const TextureAccessType textureAccess, const int32_t width,
                                       const int32_t height ) noexcept {
  return TextureUniqPtr ( create_texture< SDL_Texture * > ( renderer, pixelFormat, textureAccess, width, height ) );
}
template <>
inline TextureShrdPtr create_texture ( SDL_Renderer *renderer, const int32_t pixelFormat,
                                       const TextureAccessType textureAccess, const int32_t width,
                                       const int32_t height ) noexcept {
  return TextureShrdPtr ( create_texture< TextureUniqPtr > ( renderer, pixelFormat, textureAccess, width, height ) );
}
//
template < typename T >
T create_texture ( SDL_Window *window, SDL_Renderer *renderer, const TextureAccessType textureAccess,
                   const int32_t width, const int32_t height ) noexcept;

template <>
inline SDL_Texture *create_texture ( SDL_Window *window, SDL_Renderer *renderer, const TextureAccessType textureAccess,
                                     const int32_t width, const int32_t height ) noexcept {
  return create_texture< SDL_Texture * > ( renderer, get_window_pixelformat ( window ), textureAccess, width, height );
}
template <>
inline TextureUniqPtr create_texture ( SDL_Window *window, SDL_Renderer *renderer,
                                       const TextureAccessType textureAccess, const int32_t width,
                                       const int32_t height ) noexcept {
  return TextureUniqPtr ( create_texture< SDL_Texture * > ( window, renderer, textureAccess, width, height ) );
}
template <>
inline TextureShrdPtr create_texture ( SDL_Window *window, SDL_Renderer *renderer,
                                       const TextureAccessType textureAccess, const int32_t width,
                                       const int32_t height ) noexcept {
  return TextureShrdPtr ( create_texture< TextureUniqPtr > ( window, renderer, textureAccess, width, height ) );
}
//
template < typename T >
T create_texture ( SDL_Renderer *renderer, const char *filename, const SDL_Color &color ) noexcept;

template <>
inline SDL_Texture *create_texture ( SDL_Renderer *renderer, const char *filename, const SDL_Color &color ) noexcept {
  auto surfacePtr = create_surface< SurfaceUniqPtr > ( filename, color );
  if ( surfacePtr ) {
    return surface_to_texture< SDL_Texture * > ( renderer, std::move ( surfacePtr ) );
  }
  log::error ( log::CategoryType::Application, "Failed to create Texture." );
  return nullptr;
}

template <>
inline TextureUniqPtr create_texture ( SDL_Renderer *renderer, const char *filename, const SDL_Color &color ) noexcept {
  auto surfacePtr = create_surface< SurfaceUniqPtr > ( filename, color );
  if ( surfacePtr ) {
    return surface_to_texture< TextureUniqPtr > ( renderer, std::move ( surfacePtr ) );
  }
  log::error ( log::CategoryType::Application, "Failed to create Texture." );
  return nullptr;
}

template <>
inline TextureShrdPtr create_texture ( SDL_Renderer *renderer, const char *filename, const SDL_Color &color ) noexcept {
  auto surfacePtr = create_surface< SurfaceUniqPtr > ( filename, color );
  if ( surfacePtr ) {
    return surface_to_texture< TextureShrdPtr > ( renderer, std::move ( surfacePtr ) );
  }
  log::error ( log::CategoryType::Application, "Failed to create Texture." );
  return nullptr;
}

template < typename T >
T create_texture ( SDL_Renderer *renderer, const char *filename ) noexcept;
template <>
inline SDL_Texture *create_texture ( SDL_Renderer *renderer, const char *filename ) noexcept {
#if USE_SDL2_IMAGE
  auto ptr = IMG_LoadTexture ( renderer, filename );
#else
  auto surfacePtr           = create_surface< SurfaceUniqPtr > ( filename );
  auto ptr                  = surface_to_texture< SDL_Texture * > ( renderer, std::move ( surfacePtr ) );
#endif
  if ( !ptr ) {
    log::error ( log::CategoryType::Application, "Failed to create Texture." );
  }
  return ptr;
}
template <>
inline TextureUniqPtr create_texture ( SDL_Renderer *renderer, const char *filename ) noexcept {
  return TextureUniqPtr ( create_texture< SDL_Texture * > ( renderer, filename ) );
}
template <>
inline TextureShrdPtr create_texture ( SDL_Renderer *renderer, const char *filename ) noexcept {
  return TextureShrdPtr ( create_texture< TextureUniqPtr > ( renderer, filename ) );
}
//
template < typename T >
T create_texture ( SDL_Renderer *renderer, const uint8_t *buffer, const std::size_t size ) noexcept;
template <>
inline SDL_Texture *create_texture ( SDL_Renderer *renderer, const uint8_t *buffer,
                                     const std::size_t size ) noexcept {
  auto rw = ::SDL_RWFromConstMem ( buffer, static_cast< int > ( size ) );
  if ( rw ) {
#if USE_SDL2_IMAGE
    auto ptr = IMG_LoadTexture_RW ( renderer, rw, 1 );
#else
    auto surfacePtr = SurfaceUniqPtr ( ::SDL_LoadBMP_RW ( rw, 1 ) );
    auto ptr        = surface_to_texture< SDL_Texture * > ( renderer, std::move ( surfacePtr ) );
#endif
    if ( !ptr ) {
      log::error ( log::CategoryType::Application, "Failed to create Texture." );
    }
    return ptr;
  }
  log::error ( log::CategoryType::Application, "RWops failed to create Texture." );
  return nullptr;
}
template <>
inline TextureUniqPtr create_texture ( SDL_Renderer *renderer, const uint8_t *buffer,
                                       const std::size_t size ) noexcept {
  return TextureUniqPtr ( create_texture< SDL_Texture * > ( renderer, buffer, size ) );
}
template <>
inline TextureShrdPtr create_texture ( SDL_Renderer *renderer, const uint8_t *buffer,
                                       const std::size_t size ) noexcept {
  return TextureShrdPtr ( create_texture< TextureUniqPtr > ( renderer, buffer, size ) );
}

//
// SDL2 RENDER METHODS
//
inline void clear ( SDL_Renderer *renderer, const SDL_Color &color = { 0, 0, 0, 0 } ) noexcept {
  ::SDL_SetRenderDrawColor ( renderer, color.r, color.g, color.b, color.a );
  ::SDL_RenderClear ( renderer );
}

inline auto get_render_viewport ( SDL_Renderer *renderer ) noexcept {
  Recti32_t rec;
  ::SDL_RenderGetViewport ( renderer, &rec );
  return std::make_tuple ( rec.x, rec.y, rec.w, rec.h );
}

inline void set_render_viewport ( SDL_Renderer *renderer, const Recti32_t *rect ) noexcept {
  if ( ::SDL_RenderSetViewport ( renderer, rect ) != 0 ) {
    log::error ( log::CategoryType::Application, "Failed to set viewport." );
  }
}

inline auto get_render_scale ( SDL_Renderer *renderer ) noexcept {
  float scaleX, scaleY;
  ::SDL_RenderGetScale ( renderer, &scaleX, &scaleY );
  return std::make_tuple ( scaleX, scaleY );
}

inline void set_render_scale ( SDL_Renderer *renderer, const float x, const float y,
                               const bool enableIntegerScale = true ) noexcept {
  if ( ::SDL_RenderSetIntegerScale ( renderer, enableIntegerScale ? SDL_TRUE : SDL_FALSE ) != 0 ) {
    log::error ( log::CategoryType::Application, "Failed to set Integer scale." );
  }
  if ( ::SDL_RenderSetScale ( renderer, x, y ) != 0 ) {
    log::error ( log::CategoryType::Application, "Failed to set scale." );
  }
}

inline auto get_render_logical_size ( SDL_Renderer *renderer ) noexcept {
  int width, height;
  ::SDL_RenderGetLogicalSize ( renderer, &width, &height );
  return std::make_tuple ( width, height );
}

inline void set_render_logical_size ( SDL_Renderer *renderer, const int32_t width, const int32_t height,
                                      const bool enableIntegerScale = true ) noexcept {
  if ( ::SDL_RenderSetIntegerScale ( renderer, enableIntegerScale ? SDL_TRUE : SDL_FALSE ) != 0 ) {
    log::error ( log::CategoryType::Application, "Failed to set Integer scale." );
  }
  if ( ::SDL_RenderSetLogicalSize ( renderer, width, height ) != 0 ) {
    log::error ( log::CategoryType::Application, "Failed to set logical size." );
  }
}

inline bool is_render_clip_enabled ( SDL_Renderer *renderer ) noexcept {
  return SDL_TRUE == ::SDL_RenderIsClipEnabled ( renderer );
}
inline auto get_render_clip ( SDL_Renderer *renderer ) noexcept {
  SDL_Rect rec;
  ::SDL_RenderGetClipRect ( renderer, &rec );
  return std::make_tuple ( rec.x, rec.y, rec.w, rec.h );
}
inline void set_render_clip ( SDL_Renderer *renderer, const SDL_Rect *rect = nullptr ) noexcept {
  if ( ::SDL_RenderSetClipRect ( renderer, rect ) != 0 ) {
    log::error ( log::CategoryType::Application, "Failed to set renderer clip" );
  }
}

template < typename FUNC >
inline bool render_to_pixel ( FUNC &&func, SDL_Texture *texture, const SDL_Rect *rect = nullptr ) noexcept {
  bool rtn{ false };
  uint32_t *pixels{ nullptr };
  int pitch{ 0 };

  if ( ( rtn = ( ::SDL_LockTexture ( texture, rect, ( void ** ) &pixels, &pitch ) == 0 ) ) ) {
    func ( pixels );
    ::SDL_UnlockTexture ( texture );
  }
  return rtn;
}

inline void set_renderer_target ( SDL_Renderer *renderer, SDL_Texture *texture ) noexcept {
  if ( SDL_FALSE == ::SDL_RenderTargetSupported ( renderer ) ) {
    log::error ( log::CategoryType::Application, "Window does not support the use of render targets" );
    return;
  }
  if ( texture ) {
    int access;
    ::SDL_QueryTexture ( texture, nullptr, &access, nullptr, nullptr );
    if ( internal::to_type ( TextureAccessType::Target ) != access ) {
      log::error ( log::CategoryType::Application, "Create the texture with TextureAccessType::Target" );
    }
  }
  ::SDL_SetRenderTarget ( renderer, texture );
}

inline SDL_Texture *get_renderer_target ( SDL_Renderer *renderer ) noexcept {
  return ::SDL_GetRenderTarget ( renderer );
}

template < typename FUNC >
inline void render_to_texture ( FUNC &&func, SDL_Renderer *renderer, SDL_Texture *texture ) noexcept {
  auto currentTarget = get_renderer_target ( renderer );
  set_renderer_target ( renderer, texture );
  clear ( renderer );
  func ( );
  set_renderer_target ( renderer, currentTarget );
}

template < typename FUNC >
inline void render_call ( SDL_Renderer *renderer, FUNC &&func, const SDL_Color &color ) noexcept {
  SDL_BlendMode prevMode;
  SDL_Color prevColor;

  ::SDL_GetRenderDrawColor ( renderer, &prevColor.r, &prevColor.g, &prevColor.b, &prevColor.a );
  ::SDL_GetRenderDrawBlendMode ( renderer, &prevMode );
  ::SDL_SetRenderDrawColor ( renderer, color.r, color.g, color.b, color.a );

  auto rtn = func ( );

  ::SDL_SetRenderDrawColor ( renderer, prevColor.r, prevColor.g, prevColor.b, prevColor.a );
  ::SDL_SetRenderDrawBlendMode ( renderer, prevMode );

  if ( rtn != 0 ) {
    log::error ( log::CategoryType::Application, "Failed to Render" );
  }
}

template < typename T >
inline void render_point ( SDL_Renderer *renderer, const Point<T> &p1, const SDL_Color &color ) noexcept {
  if constexpr ( std::is_floating_point_v< T> ) {
    render_call (renderer, [ & ] ( ) { return ::SDL_RenderDrawPointF ( renderer, p1.x, p1.y ); }, color );
  } else {
    render_call (renderer, [ & ] ( ) { return ::SDL_RenderDrawPoint ( renderer, p1.x, p1.y ); }, color );
  }
}

template < typename T >
inline void render_points ( SDL_Renderer *renderer, const Point<T> *points, const std::size_t size,  const SDL_Color &color ) noexcept {
  if constexpr ( std::is_floating_point_v< T> ) {
    render_call (renderer, [ & ] ( ) { return ::SDL_RenderDrawPointsF ( renderer, points, static_cast< int > ( size ) ); }, color );
  } else {
    render_call (renderer, [ & ] ( ) { return ::SDL_RenderDrawPoints ( renderer, points, static_cast< int > ( size ) ); }, color );
  }
}

template < typename T >
inline void render_line ( SDL_Renderer *renderer, const Point<T> &p1, const Point<T>&p2,
                          const SDL_Color &color ) noexcept {
  if constexpr ( std::is_floating_point_v< T> ) {
  render_call (renderer, [ & ] ( ) { return ::SDL_RenderDrawLineF ( renderer, p1.x, p1.y, p2.x, p2.y ); }, color );
  } else {
  render_call ( renderer, [ & ] ( ) { return ::SDL_RenderDrawLine ( renderer, p1.x, p1.y, p2.x, p2.y ); }, color );
  }
}

template < typename T >
inline void render_lines ( SDL_Renderer *renderer, const Point<T> *points, const std::size_t size, const SDL_Color &color ) noexcept {
  if constexpr ( std::is_floating_point_v< T> ) {
    render_call (renderer, [ & ] ( ) { return ::SDL_RenderDrawLinesF ( renderer, points, static_cast< int > ( size ) ); }, color );
  }else {
    render_call (renderer, [ & ] ( ) { return ::SDL_RenderDrawLines ( renderer, points, static_cast< int > ( size ) ); }, color );
  }
}

template < typename T >
inline void render_rect ( SDL_Renderer *renderer, const Rect<T> &rec, const SDL_Color &color ) noexcept {
  if constexpr ( std::is_floating_point_v< T> ) {
    render_call (renderer, [ & ] ( ) { return ::SDL_RenderDrawRectF ( renderer, &rec ); }, color );
  }else {
    render_call (renderer, [ & ] ( ) { return ::SDL_RenderDrawRect ( renderer, &rec ); }, color );
  }
}

template < typename T >
inline void render_rects ( SDL_Renderer *renderer, const Rect<T> *rects, const std::size_t size, const SDL_Color &color ) noexcept {
  if constexpr ( std::is_floating_point_v< T> ) {
    render_call (renderer, [ & ] ( ) { return ::SDL_RenderDrawRectsF ( renderer, rects, static_cast< int > ( size ) ); }, color );
  }else {
    render_call (renderer, [ & ] ( ) { return ::SDL_RenderDrawRects ( renderer, rects, static_cast< int > ( size ) ); }, color );
  }
}

template < typename T >
inline void render_rect_fill ( SDL_Renderer *renderer, const Rect<T> &rec, const SDL_Color &color ) noexcept {
  if constexpr ( std::is_floating_point_v< T> ) {
    render_call (renderer, [ & ] ( ) { return ::SDL_RenderFillRectF ( renderer, &rec ); }, color );
  }else {
    render_call (renderer, [ & ] ( ) { return ::SDL_RenderFillRect ( renderer, &rec ); }, color );
  }
}

template < typename T >
inline void render_rects_fill ( SDL_Renderer *renderer, const Rect<T> *rects, const std::size_t size,
                                const SDL_Color &color ) noexcept {
  if constexpr ( std::is_floating_point_v< T> ) {
    render_call (renderer, [ & ] ( ) { return ::SDL_RenderFillRectsF ( renderer, rects, static_cast< int > ( size ) ); }, color );
   }else {
      render_call (renderer, [ & ] ( ) { return ::SDL_RenderFillRects ( renderer, rects, static_cast< int > ( size ) ); }, color );
   }
}
// We may want to convert the 'dst' parameter to a const reference
template < typename T >
inline void render_texture ( SDL_Renderer *renderer, SDL_Texture *texture,  //
                             const Recti32_t*src = nullptr,                 //
                             const Rect<T>*dst = nullptr ) noexcept {
  if constexpr ( std::is_floating_point_v< T> ) {
  int rtn = ::SDL_RenderCopyF ( renderer, texture, src, dst );
    if ( rtn != 0 ) {
      log::error ( log::CategoryType::Application, "Failed to Render Texture" );
    }
  }
  else {
    int rtn = ::SDL_RenderCopy ( renderer, texture, src, dst );
    if ( rtn != 0 ) {
      log::error ( log::CategoryType::Application, "Failed to Render Texture" );
    }
  }
}

template < typename T >
inline void render_texture ( SDL_Renderer *renderer, SDL_Texture *texture,  //
                             FlipType flip,                                 //
                             double angle                  = 0.0,           //
                             const Point<T>*angleCenter = nullptr,       //
                             const SDL_Rect *src           = nullptr,       //
                             const Rect<T> *dst          = nullptr ) noexcept {

  if constexpr ( std::is_floating_point_v< T> ) {
  int rtn = ::SDL_RenderCopyExF ( renderer,     //
                                  texture,      //
                                  src,          //
                                  dst,          //
                                  angle,        //
                                  angleCenter,  //
                                  internal::to_type< SDL_RendererFlip > ( flip ) );
  if ( rtn != 0 ) {
    log::error ( log::CategoryType::Application, "Failed to Render Texture" );
  }
  }
  else {
    int rtn = ::SDL_RenderCopyEx ( renderer,     //
                                    texture,      //
                                    src,          //
                                    dst,          //
                                    angle,        //
                                    angleCenter,  //
                                    internal::to_type< SDL_RendererFlip > ( flip ) );
    if ( rtn != 0 ) {
      log::error ( log::CategoryType::Application, "Failed to Render Texture" );
    }
  }
}

inline void render_circle ( SDL_Renderer *renderer, const Pointi32_t p, const int32_t radius,
                            const SDL_Color &color ) noexcept {
  //
  // Implements a Midpoint Circle Algorithm
  // https://en.wikipedia.org/wiki/Midpoint_circle_algorithm
  // https://stackoverflow.com/questions/38334081/howto-draw-circles-arcs-and-vector-graphics-in-sdl
  // https://stackoverflow.com/questions/8200843/predict-number-of-points-returned-mid-point-circle-algorithm
  //

  using pt_t = Pointi32_t;  // point type

  const auto diameter = ( radius * 2 );
  auto x              = ( radius - 1 );
  auto y              = 0;
  auto tx             = 1;  // change in X
  auto ty             = 1;  // change in y
  auto radiusError    = ( tx - diameter );

  std::vector< pt_t > pts;
  pts.reserve ( static_cast< std::size_t > ( floor ( ( sqrt ( 2 ) * ( radius - 1 ) + 4 ) / 2 ) * 8 ) );
  while ( x >= y ) {
    pts.emplace_back ( pt_t{ p.x + x, p.y - y } );
    pts.emplace_back ( pt_t{ p.x + x, p.y + y } );
    pts.emplace_back ( pt_t{ p.x - x, p.y - y } );
    pts.emplace_back ( pt_t{ p.x - x, p.y + y } );
    pts.emplace_back ( pt_t{ p.x + y, p.y - x } );
    pts.emplace_back ( pt_t{ p.x + y, p.y + x } );
    pts.emplace_back ( pt_t{ p.x - y, p.y - x } );
    pts.emplace_back ( pt_t{ p.x - y, p.y + x } );

    if ( radiusError <= 0 ) {
      ++y;
      radiusError += ty;
      ty += 2;
    }

    if ( radiusError > 0 ) {
      --x;
      tx += 2;
      radiusError += ( tx - diameter );
    }
  }
  render_points ( renderer, &pts[ 0 ], pts.size ( ), color );
}

#if USE_SDL2_TTF
namespace ttf {
enum class RenderModeType : int8_t
{
  Blended,  //
  Shaded,
  Solid
};

enum class StyleType : int32_t
{
  Normal        = TTF_STYLE_NORMAL,  //
  Bold          = TTF_STYLE_BOLD,
  Italic        = TTF_STYLE_ITALIC,
  Underline     = TTF_STYLE_UNDERLINE,
  Strikethrough = TTF_STYLE_STRIKETHROUGH
};
inline void quit ( ) noexcept {
  if ( isTTFEnabled ( ) ) {
    log::debug ( log::CategoryType::Application, "Quitting TTF\n" );
    TTF_Quit ( );
  }
}
inline void init ( ) noexcept {
  if ( TTF_Init ( ) != 0 ) {
    log::error ( log::CategoryType::Application, "Failed to initialize sdl2 ttf." );
  }
  std::atexit ( quit );
}
template < typename T >
T load ( const uint8_t *buffer, const std::size_t size, const int32_t pointSize,
         const int32_t index = 0 ) noexcept;
template <>
inline TTF_Font *load ( const uint8_t *buffer, std::size_t size, int pointSize, int index ) noexcept {
  auto rw = ::SDL_RWFromConstMem ( buffer, static_cast< int > ( size ) );
  if ( rw ) {
    auto ptr = TTF_OpenFontIndexRW ( rw, 1, pointSize, index );
    if ( !ptr ) {
      log::error ( log::CategoryType::Application, "Failed to create ttf." );
    }
    return ptr;
  }
  log::error ( log::CategoryType::Application, "Failed on RWops." );

  return nullptr;
}
template <>
inline TTFFontUniqPtr load ( const uint8_t *buffer, const std::size_t size, const int32_t pointSize,
                             const int32_t index ) noexcept {
  return TTFFontUniqPtr ( load< TTF_Font * > ( buffer, size, pointSize, index ) );
}
template <>
inline TTFFontShrdPtr load ( const uint8_t *buffer, const std::size_t size, const int32_t pointSize,
                             const int32_t index ) noexcept {
  return TTFFontShrdPtr ( load< TTFFontUniqPtr > ( buffer, size, pointSize, index ) );
}

template < typename T >
T load ( SDL_RWops *rw, const int32_t pointSize, const int32_t index = 0 ) noexcept;
template <>
inline TTF_Font *load ( SDL_RWops *rw, const int32_t pointSize, const int32_t index ) noexcept {
  auto ptr = TTF_OpenFontIndexRW ( rw, 0, pointSize, index );
  if ( !ptr ) {
    log::error ( log::CategoryType::Application, "Failed to create ttf." );
  }
  return ptr;
}
template <>
inline TTFFontUniqPtr load ( SDL_RWops *rw, const int32_t pointSize, const int32_t index ) noexcept {
  return TTFFontUniqPtr ( load< TTF_Font * > ( rw, pointSize, index ) );
}
template <>
inline TTFFontShrdPtr load ( SDL_RWops *rw, const int32_t pointSize, const int32_t index ) noexcept {
  return TTFFontShrdPtr ( load< TTFFontUniqPtr > ( rw, pointSize, index ) );
}

inline void set_style ( TTF_Font *font, ttf::StyleType style ) noexcept {
  if ( font ) {
    TTF_SetFontStyle ( font, internal::to_type ( style ) );
  }
}

inline void set_kerning ( TTF_Font *font, const bool enable = true ) noexcept {
  if ( font ) {
    TTF_SetFontKerning ( font, enable );
  }
}

inline auto get_height ( const TTF_Font *font ) noexcept { return font ? TTF_FontHeight ( font ) : 0; }

inline auto get_height_in_font ( const TTF_Font *font ) noexcept { return font ? TTF_FontLineSkip ( font ) : 0; }

inline auto get_text_size ( TTF_Font *font, const char *text, const bool asUTF8 = false ) noexcept {
  int32_t w{ 0 }, h{ 0 };
  if ( font ) {
    ( asUTF8 ? TTF_SizeUTF8 : TTF_SizeText ) ( font, text, &w, &h );
  }
  return std::make_tuple ( w, h );
}

inline auto get_text_size ( TTF_Font *font, const uint16_t *text ) noexcept {
  int32_t w{ 0 }, h{ 0 };
  if ( font ) {
    TTF_SizeUNICODE ( font, text, &w, &h );
  }
  return std::make_tuple ( w, h );
}

inline auto has_glyph ( TTF_Font *font, uint16_t ch ) {
  if ( font ) {
    return 0 != ::TTF_GlyphIsProvided ( font, ch );
  }
  return false;
}

inline auto get_glyph_metrics ( TTF_Font *font, uint16_t c ) {
  int minX{ 0 }, maxX{ 0 }, minY{ 0 }, maxY{ 0 }, advance{ 0 };
  if ( 0 != ::TTF_GlyphMetrics ( font, c, &minX, &maxX, &minY, &maxY, &advance ) ) {
    log::error ( log::CategoryType::Application, "Issue Fetching TTF Glyph Metrics: %s\n", TTF_GetError ( ) );
    return std::make_tuple ( 0, 0, 0, 0, 0 );
  }
  return std::make_tuple ( minX, maxX, minY, maxY, advance );
}

//
//
//
template < typename T >
T render ( TTF_Font *font, const char *text, const SDL_Color &fg, const SDL_Color &bg = { 0, 0, 0, 0 },
           const ttf::RenderModeType renderMode = ttf::RenderModeType::Solid, const bool asUTF8 = false ) noexcept;

template <>
inline SDL_Surface *render ( TTF_Font *font, const char *text, const SDL_Color &fg, const SDL_Color &bg,
                             const ttf::RenderModeType renderMode, const bool asUTF8 ) noexcept {
  SDL_Surface *surfacePtr{ nullptr };
  switch ( renderMode ) {
    case ttf::RenderModeType::Blended:
      surfacePtr = ( asUTF8 ? TTF_RenderUTF8_Blended : TTF_RenderText_Blended ) ( font, text, fg );
      break;
    case ttf::RenderModeType::Shaded:
      surfacePtr = ( asUTF8 ? TTF_RenderUTF8_Shaded : TTF_RenderText_Shaded ) ( font, text, fg, bg );
      break;
    case ttf::RenderModeType::Solid:
      surfacePtr = ( asUTF8 ? TTF_RenderUTF8_Solid : TTF_RenderText_Solid ) ( font, text, fg );
      break;
    default:
      break;
  }
  return surfacePtr;
}
template <>
inline SurfaceUniqPtr render ( TTF_Font *font, const char *text, const SDL_Color &fg, const SDL_Color &bg,
                               const ttf::RenderModeType renderMode, const bool asUTF8 ) noexcept {
  return SurfaceUniqPtr ( render< SDL_Surface * > ( font, text, fg, bg, renderMode, asUTF8 ) );
}

template <>
inline SurfaceShrdPtr render ( TTF_Font *font, const char *text, const SDL_Color &fg, const SDL_Color &bg,
                               const ttf::RenderModeType renderMode, const bool asUTF8 ) noexcept {
  return SurfaceShrdPtr ( render< SurfaceUniqPtr > ( font, text, fg, bg, renderMode, asUTF8 ) );
}
//
template < typename T >
T render ( TTF_Font *font, const uint16_t *text, const SDL_Color &fg, const SDL_Color &bg = { 0, 0, 0, 0 },
           const ttf::RenderModeType renderMode = ttf::RenderModeType::Solid ) noexcept;

template <>
inline SDL_Surface *render ( TTF_Font *font, const uint16_t *text, const SDL_Color &fg, const SDL_Color &bg,
                             const ttf::RenderModeType renderMode ) noexcept {
  SDL_Surface *surfacePtr = nullptr;
  switch ( renderMode ) {
    case ttf::RenderModeType::Blended:
      surfacePtr = ::TTF_RenderUNICODE_Blended ( font, text, fg );
      break;
    case ttf::RenderModeType::Shaded:
      surfacePtr = ::TTF_RenderUNICODE_Shaded ( font, text, fg, bg );
      break;
    case ttf::RenderModeType::Solid:
      surfacePtr = ::TTF_RenderUNICODE_Solid ( font, text, fg );
      break;
    default:
      break;
  }
  return surfacePtr;
}
template <>
inline SurfaceUniqPtr render ( TTF_Font *font, const uint16_t *text, const SDL_Color &fg, const SDL_Color &bg,
                               const ttf::RenderModeType renderMode ) noexcept {
  return SurfaceUniqPtr ( render< SDL_Surface * > ( font, text, fg, bg, renderMode ) );
}

template <>
inline SurfaceShrdPtr render ( TTF_Font *font, const uint16_t *text, const SDL_Color &fg, const SDL_Color &bg,
                               const ttf::RenderModeType renderMode ) noexcept {
  return SurfaceShrdPtr ( render< SurfaceUniqPtr > ( font, text, fg, bg, renderMode ) );
}
}  // namespace ttf

#endif

#if USE_SDL2_MIXER
namespace mixer {

enum class AudioType
{
  FLAC = MIX_INIT_FLAC,  //
  MOD  = MIX_INIT_MOD,
  MP3  = MIX_INIT_MP3,
  OGG  = MIX_INIT_OGG
};

enum class AudioOperationType
{
  Pause,  //
  Resume,
  Rewind,
  Stop
};
enum class MusicStateType
{
  Paused,  //
  Playing
};

constexpr uint8_t ALPHA_OPAQUE{ SDL_ALPHA_OPAQUE };
constexpr uint8_t ALPHA_TRANSPARENT{ SDL_ALPHA_TRANSPARENT };
constexpr auto AUDIO_VOLUME_MAX{ MIX_MAX_VOLUME };

inline void quit_audio ( ) noexcept {
  log::debug ( log::CategoryType::Application, "Quitting Audio\n" );
  Mix_CloseAudio ( );
}
inline void quit_mix ( ) noexcept {
  log::debug ( log::CategoryType::Application, "Quitting Mixer\n" );
  Mix_Quit ( );
}
template < typename... T, typename = std::enable_if_t< internal::are_same< AudioType, T... >::value, void > >
inline auto init ( const int32_t frequency, const int32_t stereochannels, const int32_t chunksize,
                   AudioType head = AudioType::OGG, T... tail ) noexcept {
  int32_t rtn = 0;
  if ( -1 == Mix_OpenAudio ( frequency, MIX_DEFAULT_FORMAT, stereochannels, chunksize ) ) {
    rtn = -1;
    log::error ( log::CategoryType::Application, "Failed to initialize SDL2_mixer." );
  }
  std::atexit ( quit_audio );
  //
  // SDL Mix init
  //
  auto flags  = internal::enum_class_or ( head, std::forward ( tail )... );
  auto result = Mix_Init ( flags );
  if ( result != flags ) {
    rtn = -1;
    log::error ( log::CategoryType::Application, "Failed to initialize SDL2_mixer." );
  }
  std::atexit ( quit_mix );
  return rtn;
}

template < typename T >
inline T load ( const char *filename ) noexcept;

template <>
inline Mix_Chunk *load ( const char *filename ) noexcept {
  auto ptr = ::Mix_LoadWAV ( filename );
  if ( !ptr ) {
    log::error ( log::CategoryType::Application, "Failed to create Mix Chunk." );
  }
  return ptr;
}
template <>
inline MixChunkUniqPtr load ( const char *filename ) noexcept {
  return MixChunkUniqPtr ( load< Mix_Chunk * > ( filename ) );
}
template <>
inline MixChunkShrdPtr load ( const char *filename ) noexcept {
  return MixChunkShrdPtr ( load< MixChunkUniqPtr > ( filename ) );
}

template <>
inline Mix_Music *load ( const char *filename ) noexcept {
  auto ptr = ::Mix_LoadMUS ( filename );
  if ( !ptr ) {
    log::error ( log::CategoryType::Application, "Failed to create Mix Music." );
  }
  return ptr;
}
template <>
inline MixMusicUniqPtr load ( const char *filename ) noexcept {
  return MixMusicUniqPtr ( load< Mix_Music * > ( filename ) );
}
template <>
inline MixMusicShrdPtr load ( const char *filename ) noexcept {
  return MixMusicShrdPtr ( load< MixMusicUniqPtr > ( filename ) );
}

template < typename T >
inline T load ( const uint8_t *buffer, int size ) noexcept;
template <>
inline Mix_Chunk *load ( const uint8_t *buffer, int size ) noexcept {
  auto rw = ::SDL_RWFromConstMem ( buffer, size );
  if ( rw ) {
    auto ptr = Mix_LoadWAV_RW ( rw, 1 );
    if ( !ptr ) {
      log::error ( log::CategoryType::Application, "Failed to create Mix Chunk." );
    }
    return ptr;
  }
  log::error ( log::CategoryType::Application, "Failed on RWops." );
  return nullptr;
}
template <>
inline MixChunkUniqPtr load ( const uint8_t *buffer, int size ) noexcept {
  return MixChunkUniqPtr ( load< Mix_Chunk * > ( buffer, size ) );
}
template <>
inline MixChunkShrdPtr load ( const uint8_t *buffer, int size ) noexcept {
  return MixChunkShrdPtr ( load< MixChunkUniqPtr > ( buffer, size ) );
}
template <>
inline Mix_Music *load ( const uint8_t *buffer, int size ) noexcept {
  auto rw = ::SDL_RWFromConstMem ( buffer, size );
  if ( rw ) {
    auto ptr = Mix_LoadMUS_RW ( rw, 1 );
    if ( !ptr ) {
      log::error ( log::CategoryType::Application, "Failed to create Mix Music." );
    }
    return ptr;
  }
  log::error ( log::CategoryType::Application, "Failed on RWops." );
  return nullptr;
}
template <>
inline MixMusicUniqPtr load ( const uint8_t *buffer, int size ) noexcept {
  return MixMusicUniqPtr ( load< Mix_Music * > ( buffer, size ) );
}
template <>
inline MixMusicShrdPtr load ( const uint8_t *buffer, int size ) noexcept {
  return MixMusicShrdPtr ( load< MixMusicUniqPtr > ( buffer, size ) );
}

//
// Sound functions
//
inline int sound_play ( Mix_Chunk *chunk, int channel = -1, int loop = 0 ) noexcept {
  int rtn = Mix_PlayChannel ( channel, chunk, loop );
  if ( -1 == rtn ) {
    log::error ( log::CategoryType::Application, "Error playing sound" );
  }
  return rtn;
}
inline void sound_allocate_channels ( int32_t num ) noexcept { Mix_AllocateChannels ( num ); }
inline int32_t sound_get_channels ( ) noexcept { return Mix_AllocateChannels ( -1 ); }
inline int sound_volume ( int channel, int volume ) noexcept { return Mix_Volume ( channel, volume ); }
inline int sound_volume_all ( int volume ) noexcept { return sound_volume ( -1, volume ); }
inline void sound_operation ( AudioOperationType operation, int channel = -1 ) noexcept {
  // channel = -1 will affect all channels
  switch ( operation ) {
    case AudioOperationType::Pause:
      Mix_Pause ( channel );
      break;
    case AudioOperationType::Resume:
      Mix_Resume ( channel );
      break;
    case AudioOperationType::Stop:
      Mix_HaltChannel ( channel );
    default:
      break;
  }
}

inline bool is_channel_playing ( int channel ) noexcept { return Mix_Playing ( channel ) != 0; }
inline int get_channels_playing ( ) noexcept { return Mix_Playing ( -1 ); }
inline void sound_callback ( void ( *callback ) ( int ) ) { Mix_ChannelFinished ( callback ); }
//
// Music functions
//
inline bool music_fading_none ( ) noexcept { return MIX_NO_FADING == Mix_FadingMusic ( ); }
inline bool music_fading_in ( ) noexcept { return MIX_FADING_IN == Mix_FadingMusic ( ); }
inline bool music_fading_out ( ) noexcept { return MIX_FADING_OUT == Mix_FadingMusic ( ); }
inline auto music_volume ( const int32_t volume ) noexcept { return Mix_VolumeMusic ( volume ); }
inline auto music_volume ( ) noexcept { return music_volume ( -1 ); }
inline void music_callback ( void ( *callback ) ( ) ) { Mix_HookMusicFinished ( callback ); }
inline bool music_state ( MusicStateType state ) noexcept {
  bool rtn{ false };
  switch ( state ) {
    case MusicStateType::Playing:
      rtn = Mix_PlayingMusic ( );
      break;
    case MusicStateType::Paused:
      rtn = Mix_PausedMusic ( );
      break;
    default:
      break;
  }
  return rtn;
}
inline bool is_music_playing ( ) noexcept { return music_state ( MusicStateType::Playing ); }
inline bool is_music_paused ( ) noexcept { return music_state ( MusicStateType::Playing ); }
inline void music_operation ( AudioOperationType operation ) noexcept {
  switch ( operation ) {
    case AudioOperationType::Rewind:
      Mix_RewindMusic ( );
      break;
    case AudioOperationType::Pause:
      Mix_PauseMusic ( );
      break;
    case AudioOperationType::Resume:
      Mix_ResumeMusic ( );
      break;
    case AudioOperationType::Stop:
      Mix_HaltMusic ( );
    default:
      break;
  }
}
inline void music_halt ( const int32_t msFade = 0 ) noexcept {
  if ( is_music_playing ( ) ) {
    ( msFade > 0 ) ? Mix_FadeOutMusic ( msFade ) : Mix_HaltMusic ( );
  }
}
inline void music_set_position ( const double position = 0.0 ) noexcept {
  Mix_RewindMusic ( );
  if ( Mix_SetMusicPosition ( position ) == -1 ) {
    log::error ( log::CategoryType::Application, "Error setting music position" );
  }
}
inline void music_play ( Mix_Music *p, const int32_t loop = 1, const bool msFadeIn = 0 ) noexcept {
  int rtn = ( msFadeIn > 0 ) ? Mix_FadeInMusic ( p, loop, msFadeIn ) : Mix_PlayMusic ( p, loop );
  if ( -1 == rtn ) {
    log::error ( log::CategoryType::Application, "Error playing music" );
  }
}
}  // namespace mixer
#endif

//
//
//
inline auto get_linked_version ( const SubsystemType subSystem ) noexcept {
  SDL_version linked{ 0, 0, 0 };
  switch ( subSystem ) {
    case SubsystemType::Core:
      SDL_GetVersion ( &linked );
      break;
    case SubsystemType::Image:
#if USE_SDL2_IMAGE
      linked = *IMG_Linked_Version ( );
#endif
      break;
    case SubsystemType::Ttf:
#if USE_SDL2_TTF
      linked = *TTF_Linked_Version ( );
#endif
      break;
    case SubsystemType::Audio:
#if USE_SDL2_MIXER
      linked = *Mix_Linked_Version ( );
#endif
      break;
    default:
      break;
  }
  return linked;
}

inline void print_info ( ) noexcept {
  // Note: eventually might want to make this easier to parse
  log::log ( "Platform %s\n", sdl2::system::platform ( ) );
  log::log ( "Cpu Count %d\n", sdl2::system::cpu::count ( ) );
  log::log ( "Ram %d\n", sdl2::system::ram ( ) );
  log::log ( "Current Video Driver %s\n", sdl2::system::video_driver ( ) );
  const auto numVideoDrivers = SDL_GetNumVideoDrivers ( );
  log::log ( "All Video Drivers " );
  for ( auto i = 0; i < numVideoDrivers; ++i ) {
    log::log ( "    %s\n", system::video_driver ( i ) );
  }

  {
    auto [ major, minor, patch ]                   = sdl2::get_version ( sdl2::SubsystemType::Core );
    auto [ linkedMajor, linkedMinor, linkedPatch ] = sdl2::get_linked_version ( sdl2::SubsystemType::Core );
    log::log ( "SDL CORE %d.%d.%d Linked %d.%d.%d", major, minor, patch, linkedMajor, linkedMinor, linkedPatch );
  }

#if USE_SDL2_TTF
  {
    auto [ major, minor, patch ]                   = sdl2::get_version ( sdl2::SubsystemType::Ttf );
    auto [ linkedMajor, linkedMinor, linkedPatch ] = sdl2::get_linked_version ( sdl2::SubsystemType::Ttf );
    log::log ( "SDL TTF %d.%d.%d Linked %d.%d.%d", major, minor, patch, linkedMajor, linkedMinor, linkedPatch );
  }
#endif

#if USE_SDL2_IMAGE
  {
    auto [ major, minor, patch ]                   = sdl2::get_version ( sdl2::SubsystemType::Image );
    auto [ linkedMajor, linkedMinor, linkedPatch ] = sdl2::get_linked_version ( sdl2::SubsystemType::Image );
    log::log ( "SDL IMAGE %d.%d.%d Linked %d.%d.%d", major, minor, patch, linkedMajor, linkedMinor, linkedPatch );
  }
#endif

#if USE_SDL2_MIXER
  {
    auto [ major, minor, patch ]                   = sdl2::get_version ( sdl2::SubsystemType::Audio );
    auto [ linkedMajor, linkedMinor, linkedPatch ] = sdl2::get_linked_version ( sdl2::SubsystemType::Audio );
    log::log ( "SDL MIXER %d.%d.%d Linked %d.%d.%d", major, minor, patch, linkedMajor, linkedMinor, linkedPatch );
  }
#endif
}

inline void print_renderer_info ( SDL_Renderer *renderer ) noexcept {
  auto [ name, texW, texH ] = get_renderer_info ( renderer );

  log::log ( "SDL RENDERER %s : %d X %d", name, texW, texH );
  int renderCount = SDL_GetNumRenderDrivers ( );
  for ( auto i{ 0 }; i < renderCount; ++i ) {
    SDL_RendererInfo info;
    if ( !SDL_GetRenderDriverInfo ( i, &info ) ) {
      log::log ( "    Name, %s", info.name );
      log::log ( "    Accelerated, %s", ( info.flags & SDL_RENDERER_ACCELERATED ) ? "true" : "false" );
    } else {
      log::log ( "    Driver Info DNE" );
    }
  }
}
inline void print_window_info ( SDL_Window *window ) noexcept {
  sdl2::log::log ( "Window ID: %d", sdl2::get_window_id ( window ) );
  sdl2::log::log ( "Window Refresh Rate: %d\n", get_window_refresh_rate ( window ) );
  const auto [ dimW, dimH ] = get_window_size ( window );
  sdl2::log::log ( "Window Size: %d X %d", dimW, dimH );
}

}  // namespace sdl2
